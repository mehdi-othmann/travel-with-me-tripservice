import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TravelWithMeTripServiceSharedModule } from 'app/shared';
import { HOME_ROUTE, HomeComponent } from './';
import { FeaturesModule } from 'app/features/features.module';

@NgModule({
    imports: [TravelWithMeTripServiceSharedModule, FeaturesModule, RouterModule.forChild([HOME_ROUTE])],
    declarations: [HomeComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TravelWithMeTripServiceHomeModule {}

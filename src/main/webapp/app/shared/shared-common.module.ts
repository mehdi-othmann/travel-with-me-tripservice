import { NgModule } from '@angular/core';

import { TravelWithMeTripServiceSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [TravelWithMeTripServiceSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [TravelWithMeTripServiceSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class TravelWithMeTripServiceSharedCommonModule {}

export interface ICity {
    id?: string;
    countryId?: string;
}

export class City implements ICity {
    constructor(public id?: string, public countryId?: string) {}
}

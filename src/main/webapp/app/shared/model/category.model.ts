import { ITrip } from 'app/shared/model/trip.model';

export interface ICategory {
    id?: string;
    name?: string;
    image?: string;
    trips?: ITrip[];
}

export class Category implements ICategory {
    constructor(public id?: string, public name?: string, public image?: string, public trips?: ITrip[]) {}
}

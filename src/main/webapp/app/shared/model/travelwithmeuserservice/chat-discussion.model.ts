import { IChatMessage } from 'app/shared/model/travelwithmeuserservice/chat-message.model';

export interface IChatDiscussion {
    id?: string;
    title?: string;
    trip?: string;
    chatmessages?: IChatMessage[];
}

export class ChatDiscussion implements IChatDiscussion {
    constructor(public id?: string, public title?: string, public trip?: string, public chatmessages?: IChatMessage[]) {}
}

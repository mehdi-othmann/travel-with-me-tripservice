export interface IChatMessage {
    id?: string;
    content?: string;
    chatDiscussionId?: string;
}

export class ChatMessage implements IChatMessage {
    constructor(public id?: string, public content?: string, public chatDiscussionId?: string) {}
}

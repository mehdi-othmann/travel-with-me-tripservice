import { IPost } from 'app/shared/model/travelwithmeproservice/post.model';

export interface IComment {
    id?: string;
    commentContents?: string;
    post?: IPost;
}

export class Comment implements IComment {
    constructor(public id?: string, public commentContents?: string, public post?: IPost) {}
}

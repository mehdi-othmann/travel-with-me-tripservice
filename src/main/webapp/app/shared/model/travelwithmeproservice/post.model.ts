import { IComment } from 'app/shared/model/travelwithmeproservice/comment.model';

export interface IPost {
    id?: string;
    postTitle?: string;
    postContents?: string;
    owner?: string;
    comments?: IComment[];
}

export class Post implements IPost {
    constructor(
        public id?: string,
        public postTitle?: string,
        public postContents?: string,
        public owner?: string,
        public comments?: IComment[]
    ) {}
}

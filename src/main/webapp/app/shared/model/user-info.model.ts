import { Moment } from 'moment';

export interface IUserInfo {
    id?: string;
    description?: string;
    birthDay?: Moment;
    job?: string;
    favoriteArtist?: string;
    favoriteBook?: string;
    userLogin?: string;
    userId?: string;
}

export class UserInfo implements IUserInfo {
    constructor(
        public id?: string,
        public description?: string,
        public birthDay?: Moment,
        public job?: string,
        public favoriteArtist?: string,
        public favoriteBook?: string,
        public userLogin?: string,
        public userId?: string
    ) {}
}

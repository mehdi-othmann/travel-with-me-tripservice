import { IUser } from 'app/core/user/user.model';

export interface ITrips {
    id?: string;
    title?: string;
    description?: string;
    countryName?: string;
    categoryName?: string;
    users?: IUser[];
    ownerLogin?: string;
    ownerId?: string;
}

export class Trips implements ITrips {
    constructor(
        public id?: string,
        public title?: string,
        public description?: string,
        public countryName?: string,
        public categoryName?: string,
        public users?: IUser[],
        public ownerLogin?: string,
        public ownerId?: string
    ) {}
}

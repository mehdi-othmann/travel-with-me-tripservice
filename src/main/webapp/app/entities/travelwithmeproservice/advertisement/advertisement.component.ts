import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IAdvertisement } from 'app/shared/model/travelwithmeproservice/advertisement.model';
import { AccountService } from 'app/core';
import { AdvertisementService } from './advertisement.service';

@Component({
    selector: 'jhi-advertisement',
    templateUrl: './advertisement.component.html'
})
export class AdvertisementComponent implements OnInit, OnDestroy {
    advertisements: IAdvertisement[];
    currentAccount: any;
    eventSubscriber: Subscription;
    currentSearch: string;

    constructor(
        protected advertisementService: AdvertisementService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected activatedRoute: ActivatedRoute,
        protected accountService: AccountService
    ) {
        this.currentSearch =
            this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search']
                ? this.activatedRoute.snapshot.params['search']
                : '';
    }

    loadAll() {
        if (this.currentSearch) {
            this.advertisementService
                .search({
                    query: this.currentSearch
                })
                .pipe(
                    filter((res: HttpResponse<IAdvertisement[]>) => res.ok),
                    map((res: HttpResponse<IAdvertisement[]>) => res.body)
                )
                .subscribe((res: IAdvertisement[]) => (this.advertisements = res), (res: HttpErrorResponse) => this.onError(res.message));
            return;
        }
        this.advertisementService
            .query()
            .pipe(
                filter((res: HttpResponse<IAdvertisement[]>) => res.ok),
                map((res: HttpResponse<IAdvertisement[]>) => res.body)
            )
            .subscribe(
                (res: IAdvertisement[]) => {
                    this.advertisements = res;
                    this.currentSearch = '';
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.currentSearch = query;
        this.loadAll();
    }

    clear() {
        this.currentSearch = '';
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInAdvertisements();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IAdvertisement) {
        return item.id;
    }

    registerChangeInAdvertisements() {
        this.eventSubscriber = this.eventManager.subscribe('advertisementListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}

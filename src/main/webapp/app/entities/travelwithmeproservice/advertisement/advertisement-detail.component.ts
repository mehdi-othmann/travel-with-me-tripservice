import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAdvertisement } from 'app/shared/model/travelwithmeproservice/advertisement.model';

@Component({
    selector: 'jhi-advertisement-detail',
    templateUrl: './advertisement-detail.component.html'
})
export class AdvertisementDetailComponent implements OnInit {
    advertisement: IAdvertisement;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ advertisement }) => {
            this.advertisement = advertisement;
        });
    }

    previousState() {
        window.history.back();
    }
}

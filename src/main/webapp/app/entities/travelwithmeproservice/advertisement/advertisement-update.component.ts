import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { IAdvertisement } from 'app/shared/model/travelwithmeproservice/advertisement.model';
import { AdvertisementService } from './advertisement.service';

@Component({
    selector: 'jhi-advertisement-update',
    templateUrl: './advertisement-update.component.html'
})
export class AdvertisementUpdateComponent implements OnInit {
    advertisement: IAdvertisement;
    isSaving: boolean;
    createdAtDp: any;
    updatedAtDp: any;
    expirationDateDp: any;

    constructor(protected advertisementService: AdvertisementService, protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ advertisement }) => {
            this.advertisement = advertisement;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.advertisement.id !== undefined) {
            this.subscribeToSaveResponse(this.advertisementService.update(this.advertisement));
        } else {
            this.subscribeToSaveResponse(this.advertisementService.create(this.advertisement));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IAdvertisement>>) {
        result.subscribe((res: HttpResponse<IAdvertisement>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IAdvertisement } from 'app/shared/model/travelwithmeproservice/advertisement.model';

type EntityResponseType = HttpResponse<IAdvertisement>;
type EntityArrayResponseType = HttpResponse<IAdvertisement[]>;

@Injectable({ providedIn: 'root' })
export class AdvertisementService {
    public resourceUrl = SERVER_API_URL + 'travelwithmeproservice/api/advertisements';
    public resourceSearchUrl = SERVER_API_URL + 'travelwithmeproservice/api/_search/advertisements';

    constructor(protected http: HttpClient) {}

    create(advertisement: IAdvertisement): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(advertisement);
        return this.http
            .post<IAdvertisement>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(advertisement: IAdvertisement): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(advertisement);
        return this.http
            .put<IAdvertisement>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: string): Observable<EntityResponseType> {
        return this.http
            .get<IAdvertisement>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IAdvertisement[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: string): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    search(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IAdvertisement[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    protected convertDateFromClient(advertisement: IAdvertisement): IAdvertisement {
        const copy: IAdvertisement = Object.assign({}, advertisement, {
            createdAt:
                advertisement.createdAt != null && advertisement.createdAt.isValid() ? advertisement.createdAt.format(DATE_FORMAT) : null,
            updatedAt:
                advertisement.updatedAt != null && advertisement.updatedAt.isValid() ? advertisement.updatedAt.format(DATE_FORMAT) : null,
            expirationDate:
                advertisement.expirationDate != null && advertisement.expirationDate.isValid()
                    ? advertisement.expirationDate.format(DATE_FORMAT)
                    : null
        });
        return copy;
    }

    protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
        if (res.body) {
            res.body.createdAt = res.body.createdAt != null ? moment(res.body.createdAt) : null;
            res.body.updatedAt = res.body.updatedAt != null ? moment(res.body.updatedAt) : null;
            res.body.expirationDate = res.body.expirationDate != null ? moment(res.body.expirationDate) : null;
        }
        return res;
    }

    protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        if (res.body) {
            res.body.forEach((advertisement: IAdvertisement) => {
                advertisement.createdAt = advertisement.createdAt != null ? moment(advertisement.createdAt) : null;
                advertisement.updatedAt = advertisement.updatedAt != null ? moment(advertisement.updatedAt) : null;
                advertisement.expirationDate = advertisement.expirationDate != null ? moment(advertisement.expirationDate) : null;
            });
        }
        return res;
    }
}

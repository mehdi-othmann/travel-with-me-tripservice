import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Advertisement } from 'app/shared/model/travelwithmeproservice/advertisement.model';
import { AdvertisementService } from './advertisement.service';
import { AdvertisementComponent } from './advertisement.component';
import { AdvertisementDetailComponent } from './advertisement-detail.component';
import { AdvertisementUpdateComponent } from './advertisement-update.component';
import { AdvertisementDeletePopupComponent } from './advertisement-delete-dialog.component';
import { IAdvertisement } from 'app/shared/model/travelwithmeproservice/advertisement.model';

@Injectable({ providedIn: 'root' })
export class AdvertisementResolve implements Resolve<IAdvertisement> {
    constructor(private service: AdvertisementService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IAdvertisement> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Advertisement>) => response.ok),
                map((advertisement: HttpResponse<Advertisement>) => advertisement.body)
            );
        }
        return of(new Advertisement());
    }
}

export const advertisementRoute: Routes = [
    {
        path: '',
        component: AdvertisementComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Advertisements'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: AdvertisementDetailComponent,
        resolve: {
            advertisement: AdvertisementResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Advertisements'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: AdvertisementUpdateComponent,
        resolve: {
            advertisement: AdvertisementResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Advertisements'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: AdvertisementUpdateComponent,
        resolve: {
            advertisement: AdvertisementResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Advertisements'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const advertisementPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: AdvertisementDeletePopupComponent,
        resolve: {
            advertisement: AdvertisementResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Advertisements'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];

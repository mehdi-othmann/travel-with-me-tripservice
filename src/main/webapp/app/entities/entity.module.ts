import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'advertisement',
                loadChildren: './travelwithmeproservice/advertisement/advertisement.module#TravelwithmeproserviceAdvertisementModule'
            },
            {
                path: 'post',
                loadChildren: './travelwithmeproservice/post/post.module#TravelwithmeproservicePostModule'
            },
            {
                path: 'comment',
                loadChildren: './travelwithmeproservice/comment/comment.module#TravelwithmeproserviceCommentModule'
            },
            {
                path: 'offer',
                loadChildren: './travelwithmeuserservice/offer/offer.module#TravelwithmeuserserviceOfferModule'
            },
            {
                path: 'chat-discussion',
                loadChildren: './travelwithmeuserservice/chat-discussion/chat-discussion.module#TravelwithmeuserserviceChatDiscussionModule'
            },
            {
                path: 'chat-message',
                loadChildren: './travelwithmeuserservice/chat-message/chat-message.module#TravelwithmeuserserviceChatMessageModule'
            },
            {
                path: 'trip',
                loadChildren: './trip/trip.module#TravelWithMeTripServiceTripModule'
            },
            {
                path: 'category',
                loadChildren: './category/category.module#TravelWithMeTripServiceCategoryModule'
            },
            {
                path: 'country',
                loadChildren: './country/country.module#TravelWithMeTripServiceCountryModule'
            },
            {
                path: 'trip',
                loadChildren: './trip/trip.module#TravelWithMeTripServiceTripModule'
            },
            {
                path: 'category',
                loadChildren: './category/category.module#TravelWithMeTripServiceCategoryModule'
            },
            {
                path: 'country',
                loadChildren: './country/country.module#TravelWithMeTripServiceCountryModule'
            },
            {
                path: 'trip',
                loadChildren: './trip/trip.module#TravelWithMeTripServiceTripModule'
            },
            {
                path: 'country',
                loadChildren: './country/country.module#TravelWithMeTripServiceCountryModule'
            },
            {
                path: 'trip',
                loadChildren: './trip/trip.module#TravelWithMeTripServiceTripModule'
            },
            {
                path: 'chat-discussion',
                loadChildren: './travelwithmeuserservice/chat-discussion/chat-discussion.module#TravelwithmeuserserviceChatDiscussionModule'
            },
            {
                path: 'chat-message',
                loadChildren: './travelwithmeuserservice/chat-message/chat-message.module#TravelwithmeuserserviceChatMessageModule'
            },
            {
                path: 'advertisement',
                loadChildren: './travelwithmeproservice/advertisement/advertisement.module#TravelwithmeproserviceAdvertisementModule'
            },
            {
                path: 'post',
                loadChildren: './travelwithmeproservice/post/post.module#TravelwithmeproservicePostModule'
            },
            {
                path: 'comment',
                loadChildren: './travelwithmeproservice/comment/comment.module#TravelwithmeproserviceCommentModule'
            },
            {
                path: 'trip',
                loadChildren: './trip/trip.module#TravelWithMeTripServiceTripModule'
            },
            {
                path: 'trip',
                loadChildren: './trip/trip.module#TravelWithMeTripServiceTripModule'
            },
            {
                path: 'category',
                loadChildren: './category/category.module#TravelWithMeTripServiceCategoryModule'
            },
            {
                path: 'country',
                loadChildren: './country/country.module#TravelWithMeTripServiceCountryModule'
            },
            {
                path: 'trip',
                loadChildren: './trip/trip.module#TravelWithMeTripServiceTripModule'
            },
            {
                path: 'category',
                loadChildren: './category/category.module#TravelWithMeTripServiceCategoryModule'
            },
            {
                path: 'trip',
                loadChildren: './trip/trip.module#TravelWithMeTripServiceTripModule'
            },
            {
                path: 'trip',
                loadChildren: './trip/trip.module#TravelWithMeTripServiceTripModule'
            },
            {
                path: 'trip',
                loadChildren: './trip/trip.module#TravelWithMeTripServiceTripModule'
            },
            {
                path: 'post',
                loadChildren: './travelwithmeproservice/post/post.module#TravelwithmeproservicePostModule'
            },
            {
                path: 'trip',
                loadChildren: './trip/trip.module#TravelWithMeTripServiceTripModule'
            },
            {
                path: 'trip',
                loadChildren: './trip/trip.module#TravelWithMeTripServiceTripModule'
            },
            {
                path: 'trip',
                loadChildren: './trip/trip.module#TravelWithMeTripServiceTripModule'
            },
            {
                path: 'trip',
                loadChildren: './trip/trip.module#TravelWithMeTripServiceTripModule'
            },
            {
                path: 'advertisement',
                loadChildren: './travelwithmeproservice/advertisement/advertisement.module#TravelwithmeproserviceAdvertisementModule'
            },
            {
                path: 'category',
                loadChildren: './category/category.module#TravelWithMeTripServiceCategoryModule'
            },
            {
                path: 'chat-discussion',
                loadChildren: './travelwithmeuserservice/chat-discussion/chat-discussion.module#TravelwithmeuserserviceChatDiscussionModule'
            },
            {
                path: 'chat-message',
                loadChildren: './travelwithmeuserservice/chat-message/chat-message.module#TravelwithmeuserserviceChatMessageModule'
            },
            {
                path: 'chat-message',
                loadChildren: './travelwithmeuserservice/chat-message/chat-message.module#TravelwithmeuserserviceChatMessageModule'
            },
            {
                path: 'user-info',
                loadChildren: './user-info/user-info.module#TravelWithMeTripServiceUserInfoModule'
            },
            {
                path: 'trip',
                loadChildren: './trip/trip.module#TravelWithMeTripServiceTripModule'
            }
            /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
        ])
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TravelWithMeTripServiceEntityModule {}

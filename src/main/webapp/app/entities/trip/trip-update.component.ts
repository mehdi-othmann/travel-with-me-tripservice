import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { JhiAlertService } from 'ng-jhipster';
import { ITrip } from 'app/shared/model/trip.model';
import { TripService } from './trip.service';
import { ICountry } from 'app/shared/model/country.model';
import { CountryService } from 'app/entities/country';
import { ICategory } from 'app/shared/model/category.model';
import { CategoryService } from 'app/entities/category';
import { IUser, UserService } from 'app/core';

@Component({
    selector: 'jhi-trip-update',
    templateUrl: './trip-update.component.html'
})
export class TripUpdateComponent implements OnInit {
    trip: ITrip;
    isSaving: boolean;

    countries: ICountry[];

    categories: ICategory[];

    users: IUser[];
    periodeDp: any;
    createdAtDp: any;
    updatedAtDp: any;

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected tripService: TripService,
        protected countryService: CountryService,
        protected categoryService: CategoryService,
        protected userService: UserService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ trip }) => {
            this.trip = trip;
        });
        this.countryService
            .query({ filter: 'trip-is-null' })
            .pipe(
                filter((mayBeOk: HttpResponse<ICountry[]>) => mayBeOk.ok),
                map((response: HttpResponse<ICountry[]>) => response.body)
            )
            .subscribe(
                (res: ICountry[]) => {
                    if (!this.trip.countryId) {
                        this.countries = res;
                    } else {
                        this.countryService
                            .find(this.trip.countryId)
                            .pipe(
                                filter((subResMayBeOk: HttpResponse<ICountry>) => subResMayBeOk.ok),
                                map((subResponse: HttpResponse<ICountry>) => subResponse.body)
                            )
                            .subscribe(
                                (subRes: ICountry) => (this.countries = [subRes].concat(res)),
                                (subRes: HttpErrorResponse) => this.onError(subRes.message)
                            );
                    }
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
        this.categoryService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ICategory[]>) => mayBeOk.ok),
                map((response: HttpResponse<ICategory[]>) => response.body)
            )
            .subscribe((res: ICategory[]) => (this.categories = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.userService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
                map((response: HttpResponse<IUser[]>) => response.body)
            )
            .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.trip.id !== undefined) {
            this.subscribeToSaveResponse(this.tripService.update(this.trip));
        } else {
            this.subscribeToSaveResponse(this.tripService.create(this.trip));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ITrip>>) {
        result.subscribe((res: HttpResponse<ITrip>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackCountryById(index: number, item: ICountry) {
        return item.id;
    }

    trackCategoryById(index: number, item: ICategory) {
        return item.id;
    }

    trackUserById(index: number, item: IUser) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

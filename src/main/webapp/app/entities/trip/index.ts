export * from './trip.service';
export * from './trip-update.component';
export * from './trip-delete-dialog.component';
export * from './trip-detail.component';
export * from './trip.component';
export * from './trip.route';

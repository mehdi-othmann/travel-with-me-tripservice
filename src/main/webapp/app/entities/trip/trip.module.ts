import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TravelWithMeTripServiceSharedModule } from 'app/shared';
import {
    TripComponent,
    TripDetailComponent,
    TripUpdateComponent,
    TripDeletePopupComponent,
    TripDeleteDialogComponent,
    tripRoute,
    tripPopupRoute
} from './';

const ENTITY_STATES = [...tripRoute, ...tripPopupRoute];

@NgModule({
    imports: [TravelWithMeTripServiceSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [TripComponent, TripDetailComponent, TripUpdateComponent, TripDeleteDialogComponent, TripDeletePopupComponent],
    entryComponents: [TripComponent, TripUpdateComponent, TripDeleteDialogComponent, TripDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TravelWithMeTripServiceTripModule {}

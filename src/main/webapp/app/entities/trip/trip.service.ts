import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ITrip } from 'app/shared/model/trip.model';

type EntityResponseType = HttpResponse<ITrip>;
type EntityArrayResponseType = HttpResponse<ITrip[]>;

@Injectable({ providedIn: 'root' })
export class TripService {
    public resourceUrl = SERVER_API_URL + 'api/trips';
    public resourceSearchUrl = SERVER_API_URL + 'api/_search/trips';

    constructor(protected http: HttpClient) {}

    create(trip: ITrip): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(trip);
        return this.http
            .post<ITrip>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(trip: ITrip): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(trip);
        return this.http
            .put<ITrip>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: string): Observable<EntityResponseType> {
        return this.http
            .get<ITrip>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<ITrip[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: string): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    search(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<ITrip[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    protected convertDateFromClient(trip: ITrip): ITrip {
        const copy: ITrip = Object.assign({}, trip, {
            periode: trip.periode != null && trip.periode.isValid() ? trip.periode.format(DATE_FORMAT) : null,
            createdAt: trip.createdAt != null && trip.createdAt.isValid() ? trip.createdAt.format(DATE_FORMAT) : null,
            updatedAt: trip.updatedAt != null && trip.updatedAt.isValid() ? trip.updatedAt.format(DATE_FORMAT) : null
        });
        return copy;
    }

    protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
        if (res.body) {
            res.body.periode = res.body.periode != null ? moment(res.body.periode) : null;
            res.body.createdAt = res.body.createdAt != null ? moment(res.body.createdAt) : null;
            res.body.updatedAt = res.body.updatedAt != null ? moment(res.body.updatedAt) : null;
        }
        return res;
    }

    protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        if (res.body) {
            res.body.forEach((trip: ITrip) => {
                trip.periode = trip.periode != null ? moment(trip.periode) : null;
                trip.createdAt = trip.createdAt != null ? moment(trip.createdAt) : null;
                trip.updatedAt = trip.updatedAt != null ? moment(trip.updatedAt) : null;
            });
        }
        return res;
    }
}

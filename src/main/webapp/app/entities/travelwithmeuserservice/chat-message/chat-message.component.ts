import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IChatMessage } from 'app/shared/model/travelwithmeuserservice/chat-message.model';
import { AccountService } from 'app/core';
import { ChatMessageService } from './chat-message.service';

@Component({
    selector: 'jhi-chat-message',
    templateUrl: './chat-message.component.html'
})
export class ChatMessageComponent implements OnInit, OnDestroy {
    chatMessages: IChatMessage[];
    currentAccount: any;
    eventSubscriber: Subscription;
    currentSearch: string;

    constructor(
        protected chatMessageService: ChatMessageService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected activatedRoute: ActivatedRoute,
        protected accountService: AccountService
    ) {
        this.currentSearch =
            this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search']
                ? this.activatedRoute.snapshot.params['search']
                : '';
    }

    loadAll() {
        if (this.currentSearch) {
            this.chatMessageService
                .search({
                    query: this.currentSearch
                })
                .pipe(
                    filter((res: HttpResponse<IChatMessage[]>) => res.ok),
                    map((res: HttpResponse<IChatMessage[]>) => res.body)
                )
                .subscribe((res: IChatMessage[]) => (this.chatMessages = res), (res: HttpErrorResponse) => this.onError(res.message));
            return;
        }
        this.chatMessageService
            .query()
            .pipe(
                filter((res: HttpResponse<IChatMessage[]>) => res.ok),
                map((res: HttpResponse<IChatMessage[]>) => res.body)
            )
            .subscribe(
                (res: IChatMessage[]) => {
                    this.chatMessages = res;
                    this.currentSearch = '';
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.currentSearch = query;
        this.loadAll();
    }

    clear() {
        this.currentSearch = '';
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInChatMessages();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IChatMessage) {
        return item.id;
    }

    registerChangeInChatMessages() {
        this.eventSubscriber = this.eventManager.subscribe('chatMessageListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}

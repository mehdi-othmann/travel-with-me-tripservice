import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TravelWithMeTripServiceSharedModule } from 'app/shared';
import {
    ChatMessageComponent,
    ChatMessageDetailComponent,
    ChatMessageUpdateComponent,
    ChatMessageDeletePopupComponent,
    ChatMessageDeleteDialogComponent,
    chatMessageRoute,
    chatMessagePopupRoute
} from './';

const ENTITY_STATES = [...chatMessageRoute, ...chatMessagePopupRoute];

@NgModule({
    imports: [TravelWithMeTripServiceSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ChatMessageComponent,
        ChatMessageDetailComponent,
        ChatMessageUpdateComponent,
        ChatMessageDeleteDialogComponent,
        ChatMessageDeletePopupComponent
    ],
    entryComponents: [ChatMessageComponent, ChatMessageUpdateComponent, ChatMessageDeleteDialogComponent, ChatMessageDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TravelwithmeuserserviceChatMessageModule {}

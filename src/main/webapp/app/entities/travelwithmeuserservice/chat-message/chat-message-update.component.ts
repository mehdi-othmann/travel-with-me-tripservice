import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { IChatMessage } from 'app/shared/model/travelwithmeuserservice/chat-message.model';
import { ChatMessageService } from './chat-message.service';

@Component({
    selector: 'jhi-chat-message-update',
    templateUrl: './chat-message-update.component.html'
})
export class ChatMessageUpdateComponent implements OnInit {
    chatMessage: IChatMessage;
    isSaving: boolean;

    constructor(protected chatMessageService: ChatMessageService, protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ chatMessage }) => {
            this.chatMessage = chatMessage;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.chatMessage.id !== undefined) {
            this.subscribeToSaveResponse(this.chatMessageService.update(this.chatMessage));
        } else {
            this.subscribeToSaveResponse(this.chatMessageService.create(this.chatMessage));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IChatMessage>>) {
        result.subscribe((res: HttpResponse<IChatMessage>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}

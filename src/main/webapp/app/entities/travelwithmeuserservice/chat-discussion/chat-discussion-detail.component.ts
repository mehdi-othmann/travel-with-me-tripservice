import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IChatDiscussion } from 'app/shared/model/travelwithmeuserservice/chat-discussion.model';

@Component({
    selector: 'jhi-chat-discussion-detail',
    templateUrl: './chat-discussion-detail.component.html'
})
export class ChatDiscussionDetailComponent implements OnInit {
    chatDiscussion: IChatDiscussion;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ chatDiscussion }) => {
            this.chatDiscussion = chatDiscussion;
        });
    }

    previousState() {
        window.history.back();
    }
}

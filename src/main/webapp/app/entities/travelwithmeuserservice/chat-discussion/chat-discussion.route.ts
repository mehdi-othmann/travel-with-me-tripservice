import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ChatDiscussion } from 'app/shared/model/travelwithmeuserservice/chat-discussion.model';
import { ChatDiscussionService } from './chat-discussion.service';
import { ChatDiscussionComponent } from './chat-discussion.component';
import { ChatDiscussionDetailComponent } from './chat-discussion-detail.component';
import { ChatDiscussionUpdateComponent } from './chat-discussion-update.component';
import { ChatDiscussionDeletePopupComponent } from './chat-discussion-delete-dialog.component';
import { IChatDiscussion } from 'app/shared/model/travelwithmeuserservice/chat-discussion.model';

@Injectable({ providedIn: 'root' })
export class ChatDiscussionResolve implements Resolve<IChatDiscussion> {
    constructor(private service: ChatDiscussionService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IChatDiscussion> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<ChatDiscussion>) => response.ok),
                map((chatDiscussion: HttpResponse<ChatDiscussion>) => chatDiscussion.body)
            );
        }
        return of(new ChatDiscussion());
    }
}

export const chatDiscussionRoute: Routes = [
    {
        path: '',
        component: ChatDiscussionComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ChatDiscussions'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: ChatDiscussionDetailComponent,
        resolve: {
            chatDiscussion: ChatDiscussionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ChatDiscussions'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: ChatDiscussionUpdateComponent,
        resolve: {
            chatDiscussion: ChatDiscussionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ChatDiscussions'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: ChatDiscussionUpdateComponent,
        resolve: {
            chatDiscussion: ChatDiscussionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ChatDiscussions'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const chatDiscussionPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: ChatDiscussionDeletePopupComponent,
        resolve: {
            chatDiscussion: ChatDiscussionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ChatDiscussions'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];

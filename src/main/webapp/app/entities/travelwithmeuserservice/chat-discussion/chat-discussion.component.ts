import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IChatDiscussion } from 'app/shared/model/travelwithmeuserservice/chat-discussion.model';
import { AccountService } from 'app/core';
import { ChatDiscussionService } from './chat-discussion.service';

@Component({
    selector: 'jhi-chat-discussion',
    templateUrl: './chat-discussion.component.html'
})
export class ChatDiscussionComponent implements OnInit, OnDestroy {
    chatDiscussions: IChatDiscussion[];
    currentAccount: any;
    eventSubscriber: Subscription;
    currentSearch: string;

    constructor(
        protected chatDiscussionService: ChatDiscussionService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected activatedRoute: ActivatedRoute,
        protected accountService: AccountService
    ) {
        this.currentSearch =
            this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search']
                ? this.activatedRoute.snapshot.params['search']
                : '';
    }

    loadAll() {
        if (this.currentSearch) {
            this.chatDiscussionService
                .search({
                    query: this.currentSearch
                })
                .pipe(
                    filter((res: HttpResponse<IChatDiscussion[]>) => res.ok),
                    map((res: HttpResponse<IChatDiscussion[]>) => res.body)
                )
                .subscribe((res: IChatDiscussion[]) => (this.chatDiscussions = res), (res: HttpErrorResponse) => this.onError(res.message));
            return;
        }
        this.chatDiscussionService
            .query()
            .pipe(
                filter((res: HttpResponse<IChatDiscussion[]>) => res.ok),
                map((res: HttpResponse<IChatDiscussion[]>) => res.body)
            )
            .subscribe(
                (res: IChatDiscussion[]) => {
                    this.chatDiscussions = res;
                    this.currentSearch = '';
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.currentSearch = query;
        this.loadAll();
    }

    clear() {
        this.currentSearch = '';
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInChatDiscussions();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IChatDiscussion) {
        return item.id;
    }

    registerChangeInChatDiscussions() {
        this.eventSubscriber = this.eventManager.subscribe('chatDiscussionListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IChatDiscussion } from 'app/shared/model/travelwithmeuserservice/chat-discussion.model';

type EntityResponseType = HttpResponse<IChatDiscussion>;
type EntityArrayResponseType = HttpResponse<IChatDiscussion[]>;

@Injectable({ providedIn: 'root' })
export class ChatDiscussionService {
    public resourceUrl = SERVER_API_URL + 'travelwithmeuserservice/api/chat-discussions';
    public resourceSearchUrl = SERVER_API_URL + 'travelwithmeuserservice/api/_search/chat-discussions';

    constructor(protected http: HttpClient) {}

    create(chatDiscussion: IChatDiscussion): Observable<EntityResponseType> {
        return this.http.post<IChatDiscussion>(this.resourceUrl, chatDiscussion, { observe: 'response' });
    }

    update(chatDiscussion: IChatDiscussion): Observable<EntityResponseType> {
        return this.http.put<IChatDiscussion>(this.resourceUrl, chatDiscussion, { observe: 'response' });
    }

    find(id: string): Observable<EntityResponseType> {
        return this.http.get<IChatDiscussion>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IChatDiscussion[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: string): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    search(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IChatDiscussion[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
    }
}

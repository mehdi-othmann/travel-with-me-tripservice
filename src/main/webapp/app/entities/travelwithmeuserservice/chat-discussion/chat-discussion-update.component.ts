import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { IChatDiscussion } from 'app/shared/model/travelwithmeuserservice/chat-discussion.model';
import { ChatDiscussionService } from './chat-discussion.service';

@Component({
    selector: 'jhi-chat-discussion-update',
    templateUrl: './chat-discussion-update.component.html'
})
export class ChatDiscussionUpdateComponent implements OnInit {
    chatDiscussion: IChatDiscussion;
    isSaving: boolean;

    constructor(protected chatDiscussionService: ChatDiscussionService, protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ chatDiscussion }) => {
            this.chatDiscussion = chatDiscussion;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.chatDiscussion.id !== undefined) {
            this.subscribeToSaveResponse(this.chatDiscussionService.update(this.chatDiscussion));
        } else {
            this.subscribeToSaveResponse(this.chatDiscussionService.create(this.chatDiscussion));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IChatDiscussion>>) {
        result.subscribe((res: HttpResponse<IChatDiscussion>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}

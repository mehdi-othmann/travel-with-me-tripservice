import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TravelWithMeTripServiceSharedModule } from 'app/shared';
import {
    ChatDiscussionComponent,
    ChatDiscussionDetailComponent,
    ChatDiscussionUpdateComponent,
    ChatDiscussionDeletePopupComponent,
    ChatDiscussionDeleteDialogComponent,
    chatDiscussionRoute,
    chatDiscussionPopupRoute
} from './';

const ENTITY_STATES = [...chatDiscussionRoute, ...chatDiscussionPopupRoute];

@NgModule({
    imports: [TravelWithMeTripServiceSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ChatDiscussionComponent,
        ChatDiscussionDetailComponent,
        ChatDiscussionUpdateComponent,
        ChatDiscussionDeleteDialogComponent,
        ChatDiscussionDeletePopupComponent
    ],
    entryComponents: [
        ChatDiscussionComponent,
        ChatDiscussionUpdateComponent,
        ChatDiscussionDeleteDialogComponent,
        ChatDiscussionDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TravelwithmeuserserviceChatDiscussionModule {}

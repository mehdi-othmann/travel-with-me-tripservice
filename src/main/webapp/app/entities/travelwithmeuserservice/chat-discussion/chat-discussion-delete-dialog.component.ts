import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IChatDiscussion } from 'app/shared/model/travelwithmeuserservice/chat-discussion.model';
import { ChatDiscussionService } from './chat-discussion.service';

@Component({
    selector: 'jhi-chat-discussion-delete-dialog',
    templateUrl: './chat-discussion-delete-dialog.component.html'
})
export class ChatDiscussionDeleteDialogComponent {
    chatDiscussion: IChatDiscussion;

    constructor(
        protected chatDiscussionService: ChatDiscussionService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.chatDiscussionService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'chatDiscussionListModification',
                content: 'Deleted an chatDiscussion'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-chat-discussion-delete-popup',
    template: ''
})
export class ChatDiscussionDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ chatDiscussion }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(ChatDiscussionDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.chatDiscussion = chatDiscussion;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/chat-discussion', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/chat-discussion', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}

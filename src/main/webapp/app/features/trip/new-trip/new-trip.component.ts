import { Component, OnInit } from '@angular/core';
import { IUser, UserService } from 'app/core';
import { JhiAlertService } from 'ng-jhipster';
import { CountryService } from 'app/entities/country';
import { CategoryService } from 'app/entities/category';
import { ActivatedRoute } from '@angular/router';
import { filter, map } from 'rxjs/operators';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ITrip } from 'app/features/model/trip.model';
import { ICountry } from 'app/features/model/country.model';
import { ICategory } from 'app/features/model/category.model';
import { TripsService } from 'app/features/trip/trips.service';

@Component({
    selector: 'app-new-trip',
    templateUrl: './new-trip.component.html',
    styles: []
})
export class NewTripComponent implements OnInit {
    trip: ITrip;
    isSaving: boolean;

    countries: ICountry[];

    categories: ICategory[];

    users: IUser[];
    periodeDp: any;
    createdAtDp: any;
    updatedAtDp: any;

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected tripsService: TripsService,
        protected countryService: CountryService,
        protected categoryService: CategoryService,
        protected userService: UserService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ trip }) => {
            this.trip = trip;
        });
        this.countryService
            .query({ filter: 'trip-is-null' })
            .pipe(
                filter((mayBeOk: HttpResponse<ICountry[]>) => mayBeOk.ok),
                map((response: HttpResponse<ICountry[]>) => response.body)
            )
            .subscribe(
                (res: ICountry[]) => {
                    if (!this.trip.countryId) {
                        this.countries = res;
                    } else {
                        this.countryService
                            .find(this.trip.countryId)
                            .pipe(
                                filter((subResMayBeOk: HttpResponse<ICountry>) => subResMayBeOk.ok),
                                map((subResponse: HttpResponse<ICountry>) => subResponse.body)
                            )
                            .subscribe(
                                (subRes: ICountry) => (this.countries = [subRes].concat(res)),
                                (subRes: HttpErrorResponse) => this.onError(subRes.message)
                            );
                    }
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
        this.categoryService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ICategory[]>) => mayBeOk.ok),
                map((response: HttpResponse<ICategory[]>) => response.body)
            )
            .subscribe((res: ICategory[]) => (this.categories = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.userService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
                map((response: HttpResponse<IUser[]>) => response.body)
            )
            .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.subscribeToSaveResponse(this.tripsService.create(this.trip));
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ITrip>>) {
        result.subscribe((res: HttpResponse<ITrip>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackCountryById(index: number, item: ICountry) {
        return item.id;
    }

    trackCategoryById(index: number, item: ICategory) {
        return item.id;
    }

    trackUserById(index: number, item: IUser) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

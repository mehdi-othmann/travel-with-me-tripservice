import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { ITrip } from 'app/features/model/trip.model';
import { createRequestOption, DATE_FORMAT } from 'app/shared';
import { Injectable } from '@angular/core';
import { JoinTrip } from 'app/features/model/joinTrip.model';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { FilterTrip } from 'app/features/model/form/filterTrip.model';
import { ICategory } from 'app/shared/model/category.model';
import { ITrips } from 'app/shared/model/trips.model';

type EntityResponseType = HttpResponse<ITrip>;
type EntityArrayResponseType = HttpResponse<ITrip[]>;

@Injectable({ providedIn: 'root' })
export class TripsService {
    public resourceUrl = SERVER_API_URL + 'api/voyages';
    public ressourUrlCat = SERVER_API_URL + 'api/category';
    public resourceSearchUrl = SERVER_API_URL + 'api/_search/voyages';

    constructor(protected http: HttpClient) {}

    filter(filterTrip: FilterTrip): Observable<EntityResponseType> {
        return this.http.post(`${this.resourceUrl}/filter`, filterTrip, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ITrip[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    getUserVoyage(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);

        return this.http.get<ITrip[]>(`${this.resourceUrl}/user`, { params: options, observe: 'response' });
    }

    find(id: string): Observable<EntityResponseType> {
        return this.http.get<ITrip>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    joinTrip(joinTripDto: JoinTrip) {
        return this.http.post(`${this.resourceUrl}/joinTrip`, joinTripDto, { observe: 'response' });
    }

    create(trip: ITrip): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(trip);
        return this.http
            .post<ITrip>(`${this.resourceUrl}/new`, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    findCategories() {
        return this.http.get<ICategory[]>(`${this.ressourUrlCat}/home`, { observe: 'response' });
    }

    search(req?: any): Observable<EntityArrayResponseType> {
        return this.http.get<ITrip[]>(`${this.resourceSearchUrl}/${req}`, { observe: 'response' });
    }

    protected convertDateFromClient(trip: ITrip): ITrip {
        const copy: ITrip = Object.assign({}, trip, {
            periode: trip.periode != null && trip.periode.isValid() ? trip.periode.format(DATE_FORMAT) : null,
            createdAt: trip.createdAt != null && trip.createdAt.isValid() ? trip.createdAt.format(DATE_FORMAT) : null,
            updatedAt: trip.updatedAt != null && trip.updatedAt.isValid() ? trip.updatedAt.format(DATE_FORMAT) : null
        });
        return copy;
    }

    protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
        if (res.body) {
            res.body.periode = res.body.periode != null ? moment(res.body.periode) : null;
            res.body.createdAt = res.body.createdAt != null ? moment(res.body.createdAt) : null;
            res.body.updatedAt = res.body.updatedAt != null ? moment(res.body.updatedAt) : null;
        }
        return res;
    }

    protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        if (res.body) {
            res.body.forEach((trip: ITrip) => {
                trip.periode = trip.periode != null ? moment(trip.periode) : null;
                trip.createdAt = trip.createdAt != null ? moment(trip.createdAt) : null;
                trip.updatedAt = trip.updatedAt != null ? moment(trip.updatedAt) : null;
            });
        }
        return res;
    }
}

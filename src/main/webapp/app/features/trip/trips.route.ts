import { ActivatedRouteSnapshot, Resolve, Route, RouterStateSnapshot } from '@angular/router';

import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { TripsService } from 'app/features/trip/trips.service';

import { EditTripComponent } from 'app/features/trip/edit-trip/edit-trip.component';
import { UserRouteAccessService } from 'app/core';
import { MyTripsComponent } from 'app/features/trip/my-trips/my-trips.component';
import { ITrip, Trip } from 'app/features/model/trip.model';
import { TripsDetailsComponent } from 'app/features/trip/trips-details/trips-details.component';
import { TripsComponent } from 'app/features/trip/trips/trips.component';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { TripResolve, TripUpdateComponent } from 'app/entities/trip';
import { NewTripComponent } from 'app/features/trip/new-trip/new-trip.component';
import { FilterTripsComponent } from 'app/features/trip/filter-trips/filter-trips.component';

@Injectable({ providedIn: 'root' })
export class TripsResolve implements Resolve<ITrip> {
    constructor(private service: TripsService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ITrip> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Trip>) => response.ok),
                map((trip: HttpResponse<Trip>) => trip.body)
            );
        }
        return of(new Trip());
    }
}

export const TRIPS_ROUTE: Route[] = [
    {
        path: 'voyages',
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        component: TripsComponent,
        data: {
            defaultSort: 'id,asc',
            pageTitle: 'Voyages'
        }
    },
    {
        path: 'voyages/filter',
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        component: FilterTripsComponent,
        data: {
            defaultSort: 'id,asc',
            pageTitle: 'Voyages'
        }
    },
    {
        path: 'voyages/:id/view',
        component: TripsDetailsComponent,
        resolve: {
            trip: TripsResolve
        }
    },
    {
        path: 'voyages/:id/edit',
        component: EditTripComponent,
        resolve: {
            trip: TripsResolve
        },
        data: {
            authorities: [],
            pageTitle: 'Trips'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'mes-voyages',
        component: MyTripsComponent,
        data: {
            authorities: ['ROLE_USER', 'ROLE_PRO', 'ROLE_CLIENT'],
            pageTitle: 'Trips'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new/trip',
        component: NewTripComponent,
        resolve: {
            trip: TripsResolve
        },
        data: {
            authorities: ['ROLE_CLIENT', 'ROLE_PRO'],
            pageTitle: 'Trips'
        },
        canActivate: [UserRouteAccessService]
    }
];

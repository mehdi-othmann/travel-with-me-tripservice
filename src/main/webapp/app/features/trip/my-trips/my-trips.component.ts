import { Component, OnInit } from '@angular/core';
import { filter, map } from 'rxjs/operators';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';

import { ITrip } from 'app/features/model/trip.model';
import { AccountService } from 'app/core';
import { TripsService } from 'app/features/trip/trips.service';

@Component({
    selector: 'app-my-trips',
    templateUrl: './my-trips.component.html',
    styles: []
})
export class MyTripsComponent implements OnInit {
    trips: ITrip[];
    currentUserId: any;

    constructor(protected tripsService: TripsService, private accountService: AccountService) {}

    ngOnInit() {
        this.accountService.identity().then(account => {
            this.loadAll(account.id);
        });
    }

    loadAll(currentUserId) {
        this.currentUserId = currentUserId;
        this.tripsService
            .getUserVoyage()
            .pipe(
                filter((res: HttpResponse<ITrip[]>) => res.ok),
                map((res: HttpResponse<ITrip[]>) => res.body)
            )
            .subscribe(
                (res: ITrip[]) => {
                    this.trips = res;
                },
                (res: HttpErrorResponse) => console.log(res)
            );
    }
}

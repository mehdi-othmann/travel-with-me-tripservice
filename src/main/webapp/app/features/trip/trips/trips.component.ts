import { Component, OnInit } from '@angular/core';

import { filter, map } from 'rxjs/operators';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ITrip } from 'app/features/model/trip.model';
import { TripsService } from 'app/features/trip/trips.service';
import { IUser, UserService } from 'app/core';
import { AccountService } from 'app/core';
import { ITEMS_PER_PAGE } from 'app/shared';
import { ActivatedRoute, Router } from '@angular/router';
import { JhiAlertService, JhiEventManager, JhiParseLinks } from 'ng-jhipster';
import { JoinTrip } from 'app/features/model/joinTrip.model';
import { FilterTrip, IFilterTrip } from 'app/features/model/form/filterTrip.model';
import { ICountry } from 'app/features/model/country.model';
import { CountrysService } from 'app/features/country/countrys.service';
import { IAdvertisement } from 'app/features/model/travelwithmeproservice/advertisement.model';

@Component({
    selector: 'app-trips',
    templateUrl: './trips.component.html',
    styles: []
})
export class TripsComponent implements OnInit {
    filterTrip = new FilterTrip('', 0, 0, '', '');
    trips: ITrip[];
    countries: ICountry[];
    currentUserLogin: any;
    routeData: any;
    links: any;
    totalItems: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    error: any;
    success: any;
    currentSearch: string;

    constructor(
        protected tripsService: TripsService,
        private accountService: AccountService,
        protected userService: UserService,
        protected router: Router,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected parseLinks: JhiParseLinks,
        protected activatedRoute: ActivatedRoute,
        protected countryService: CountrysService
    ) {
        this.itemsPerPage = 9;
        this.routeData = this.activatedRoute.data.subscribe(data => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
    }

    ngOnInit() {
        this.loadAll();
        if (this.accountService.isAuthenticated()) {
            this.accountService.identity().then(account => {
                this.setcurrentUserId(account.login);
            });
        }
    }

    setcurrentUserId(login) {
        this.currentUserLogin = login;
    }

    loadAll() {
        this.countryService
            .query()
            .pipe(
                filter((res: HttpResponse<ICountry[]>) => res.ok),
                map((res: HttpResponse<ICountry[]>) => res.body)
            )
            .subscribe(
                (res: ICountry[]) => {
                    this.countries = res;
                },
                (res: HttpErrorResponse) => console.log('error')
            );
        this.tripsService
            .query({
                page: this.page - 1,
                size: this.itemsPerPage,
                sort: this.sort()
            })
            .subscribe(
                (res: HttpResponse<ITrip[]>) => this.paginateTrips(res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/voyages'], {
            queryParams: {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;

        this.router.navigate([
            '/voyages',
            {
                page: this.page,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        ]);
        this.loadAll();
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    protected paginateTrips(data: ITrip[], headers: HttpHeaders) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
        this.trips = data;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    filter() {
        this.tripsService
            .filter(this.filterTrip)
            .subscribe((res: any) => (this.trips = res.body), (res: HttpErrorResponse) => this.onError(res.message));
        console.log(this.trips);
    }

    refresh() {
        this.filterTrip = new FilterTrip('', 0, 0, '', '');
        this.loadAll();
    }
}

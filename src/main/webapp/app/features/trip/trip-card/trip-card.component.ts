import { Component, Input, OnInit } from '@angular/core';
import { ITrip } from 'app/features/model/trip.model';
import { JoinTrip } from 'app/features/model/joinTrip.model';
import { filter, map } from 'rxjs/operators';
import { TripsService } from 'app/features/trip/trips.service';
import { AccountService } from 'app/core';
import { JhiAlertService } from 'ng-jhipster';

@Component({
    selector: 'app-trip-card',
    templateUrl: './trip-card.component.html',
    styles: []
})
export class TripCardComponent implements OnInit {
    @Input() trip: ITrip;
    currentUserLogin: any;

    constructor(
        protected tripsService: TripsService,
        protected accountService: AccountService,
        protected jhiAlertService: JhiAlertService
    ) {}

    ngOnInit() {
        if (this.accountService.isAuthenticated()) {
            this.accountService.identity().then(account => {
                this.setcurrentUserId(account.login);
            });
        }
    }

    setcurrentUserId(login) {
        this.currentUserLogin = login;
    }

    joinTrip(tripId) {
        if (this.accountService.isAuthenticated()) {
            const joinTripDto = new JoinTrip(tripId, this.currentUserLogin);
            this.tripsService
                .joinTrip(joinTripDto)
                .pipe(
                    filter(res => res.ok),
                    map(res => res.body)
                )
                .subscribe(
                    res => {
                        console.log(res);
                    },
                    res => console.log(res)
                );
        } else {
            this.jhiAlertService.error('You need an account, please login or register', null, null);
        }
    }

    hasJoinedTrip(trip) {
        let isParticipant = false;
        for (let participant of trip.users) {
            if (participant.login === this.currentUserLogin) {
                isParticipant = true;
            }
        }
        return trip.ownerLogin === this.currentUserLogin || isParticipant;
    }
}

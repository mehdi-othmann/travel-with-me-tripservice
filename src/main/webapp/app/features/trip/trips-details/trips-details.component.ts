import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ITrip } from 'app/features/model/trip.model';

@Component({
    selector: 'app-trips-details',
    templateUrl: './trips-details.component.html',
    styles: []
})
export class TripsDetailsComponent implements OnInit {
    trip: ITrip;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ trip }) => {
            this.trip = trip;
        });
    }

    previousState() {
        window.history.back();
    }
}

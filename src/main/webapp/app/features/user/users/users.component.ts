import { Component, OnInit } from '@angular/core';
import { IUserInfo } from 'app/shared/model/user-info.model';
import { filter, map } from 'rxjs/operators';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { UserService } from 'app/core';
import { UsersService } from 'app/features/user/users.service';

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
    styles: []
})
export class UsersComponent implements OnInit {
    userInfos: IUserInfo[];
    constructor(private usersService: UsersService) {}

    ngOnInit() {
        this.loadAll();
    }

    loadAll() {
        this.usersService
            .query()
            .pipe(
                filter((res: HttpResponse<IUserInfo[]>) => res.ok),
                map((res: HttpResponse<IUserInfo[]>) => res.body)
            )
            .subscribe(
                (res: IUserInfo[]) => {
                    this.userInfos = res;
                },
                (res: HttpErrorResponse) => console.log(res)
            );
    }
}

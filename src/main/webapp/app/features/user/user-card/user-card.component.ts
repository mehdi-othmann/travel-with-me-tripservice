import { Component, Input, OnInit } from '@angular/core';
import { ITrip } from 'app/features/model/trip.model';
import { IUser } from 'app/features/model/user.model';

@Component({
    selector: 'app-user-card',
    templateUrl: './user-card.component.html',
    styles: []
})
export class UserCardComponent implements OnInit {
    @Input() user: IUser;

    constructor() {}

    ngOnInit() {}
}

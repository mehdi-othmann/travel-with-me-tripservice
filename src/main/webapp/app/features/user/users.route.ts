import { ActivatedRouteSnapshot, Resolve, Route, RouterStateSnapshot } from '@angular/router';

import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { TripsService } from 'app/features/trip/trips.service';

import { EditTripComponent } from 'app/features/trip/edit-trip/edit-trip.component';
import { UserRouteAccessService } from 'app/core';
import { MyTripsComponent } from 'app/features/trip/my-trips/my-trips.component';
import { ITrip, Trip } from 'app/features/model/trip.model';
import { TripsDetailsComponent } from 'app/features/trip/trips-details/trips-details.component';
import { TripsComponent } from 'app/features/trip/trips/trips.component';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { TripResolve, TripUpdateComponent } from 'app/entities/trip';
import { NewTripComponent } from 'app/features/trip/new-trip/new-trip.component';
import { FilterTripsComponent } from 'app/features/trip/filter-trips/filter-trips.component';
import { UsersComponent } from 'app/features/user/users/users.component';

export const USERS_ROUTE: Route[] = [
    {
        path: 'utilisateurs',
        component: UsersComponent,
        data: {
            defaultSort: 'id,asc',
            pageTitle: 'Voyages'
        }
    }
];

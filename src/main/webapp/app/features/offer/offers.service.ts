import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';

import { IOffer } from 'app/shared/model/travelwithmeuserservice/offer.model';
import { createRequestOption } from 'app/shared';

type EntityResponseType = HttpResponse<IOffer>;
type EntityArrayResponseType = HttpResponse<IOffer[]>;

@Injectable({ providedIn: 'root' })
export class OffersService {
    public resourceUrl = SERVER_API_URL + 'travelwithmeuserservice/api/offers';
    public resourceSearchUrl = SERVER_API_URL + 'travelwithmeuserservice/api/_search/offers';

    constructor(protected http: HttpClient) {}

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IOffer[]>(this.resourceUrl, { params: options, observe: 'response' });
    }
}

import { Component, OnInit } from '@angular/core';

import { filter, map } from 'rxjs/operators';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { OffersService } from 'app/features/offer/offers.service';
import { IOffer } from 'app/shared/model/travelwithmeuserservice/offer.model';

@Component({
    selector: 'app-offers',
    templateUrl: './offers.component.html',
    styles: []
})
export class OffersComponent implements OnInit {
    offers: IOffer[];

    constructor(protected offersService: OffersService) {}

    ngOnInit() {
        this.loadAll();
    }

    loadAll() {
        this.offersService
            .query()
            .pipe(
                filter((res: HttpResponse<IOffer[]>) => res.ok),
                map((res: HttpResponse<IOffer[]>) => res.body)
            )
            .subscribe(
                (res: IOffer[]) => {
                    this.offers = res;
                },
                (res: HttpErrorResponse) => console.log(res)
            );
    }
}

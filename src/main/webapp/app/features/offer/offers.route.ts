import { Route } from '@angular/router';
import { OffersComponent } from 'app/features/offer/offers/offers.component';

export const OFFERS_ROUTE: Route[] = [
    {
        path: 'offres',
        component: OffersComponent
    }
];

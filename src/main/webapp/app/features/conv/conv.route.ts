import { ConvService } from 'app/features/conv/conv.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Route, RouterStateSnapshot } from '@angular/router';
import { ChatDiscussion, IChatDiscussion } from 'app/shared/model/travelwithmeuserservice/chat-discussion.model';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { MyConvComponent } from 'app/features/conv/my-conv/my-conv.component';
import { ConvDetailsComponent } from 'app/features/conv/conv-details/conv-details.component';
import { TripsResolve } from 'app/features/trip';

@Injectable({ providedIn: 'root' })
export class ConvResolve implements Resolve<IChatDiscussion> {
    constructor(private service: ConvService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IChatDiscussion> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<ChatDiscussion>) => response.ok),
                map((trip: HttpResponse<ChatDiscussion>) => trip.body)
            );
        }
        return of(new ChatDiscussion());
    }
}

export const CONV_ROUTE: Route[] = [
    {
        path: 'mes-conversation',
        component: MyConvComponent
    },
    {
        path: 'conv/:id/view',
        component: ConvDetailsComponent
    }
];

import { Component, OnInit } from '@angular/core';
import { IChatDiscussion } from 'app/shared/model/travelwithmeuserservice/chat-discussion.model';
import { ConvService } from 'app/features/conv/conv.service';
import { AccountService } from 'app/core';
import { filter, map } from 'rxjs/operators';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { IAdvertisement } from 'app/features/model/travelwithmeproservice/advertisement.model';

@Component({
    selector: 'app-my-conv',
    templateUrl: './my-conv.component.html',
    styles: []
})
export class MyConvComponent implements OnInit {
    chatDiscussions: IChatDiscussion[];

    constructor(protected convService: ConvService, private accountService: AccountService) {}

    loadAll() {
        this.convService
            .getUserConv()
            .pipe(
                filter((res: HttpResponse<IChatDiscussion[]>) => res.ok),
                map((res: HttpResponse<IChatDiscussion[]>) => res.body)
            )
            .subscribe(
                (res: IChatDiscussion[]) => {
                    this.chatDiscussions = res;
                },
                (res: HttpErrorResponse) => console.log(res)
            );
    }

    ngOnInit() {
        this.loadAll();
    }
}

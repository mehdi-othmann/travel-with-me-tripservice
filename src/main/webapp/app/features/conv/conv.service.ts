import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { Injectable } from '@angular/core';
import { IChatDiscussion } from 'app/shared/model/travelwithmeuserservice/chat-discussion.model';
import { IChatMessage } from 'app/features/model/travelwithmeuserservice/chat-message.model';

type EntityArrayResponseType = HttpResponse<IChatDiscussion[]>;
type EntityResponseType = HttpResponse<IChatDiscussion>;

@Injectable({ providedIn: 'root' })
export class ConvService {
    public resourceUrl = SERVER_API_URL + 'travelwithmeuserservice/api/chat-discussions';

    public resourceUrlMessage = SERVER_API_URL + 'travelwithmeuserservice/api/chat-message';

    public resourceUrlGateway = SERVER_API_URL + 'api/voyages';

    constructor(protected http: HttpClient) {}

    getUserConv(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);

        return this.http.get<IChatDiscussion[]>(`${this.resourceUrlGateway}/userDiscussions`, { params: options, observe: 'response' });
    }

    find(id: string): Observable<EntityResponseType> {
        return this.http.get<IChatDiscussion>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    getUserConvDetail(id: string): Observable<EntityArrayResponseType> {
        return this.http.get<IChatMessage[]>(`${this.resourceUrlMessage}/chat-discussion/${id}`, { observe: 'response' });
    }
}

import { Component, OnInit } from '@angular/core';
import { ITrip } from 'app/features/model/trip.model';
import { ActivatedRoute } from '@angular/router';
import { IChatMessage } from 'app/features/model/travelwithmeuserservice/chat-message.model';
import { ConvService } from 'app/features/conv/conv.service';
import { filter, map } from 'rxjs/operators';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { IOffer } from 'app/shared/model/travelwithmeuserservice/offer.model';

@Component({
    selector: 'app-conv-details',
    templateUrl: './conv-details.component.html',
    styles: []
})
export class ConvDetailsComponent implements OnInit {
    messages: IChatMessage[];

    constructor(protected activatedRoute: ActivatedRoute, protected convService: ConvService) {}

    ngOnInit() {
        this.convService
            .getUserConvDetail(this.activatedRoute.snapshot.paramMap.get('id'))
            .pipe(
                filter((res: HttpResponse<IChatMessage[]>) => res.ok),
                map((res: HttpResponse<IChatMessage[]>) => res.body)
            )
            .subscribe(
                (res: IChatMessage[]) => {
                    this.messages = res;
                },
                (res: HttpErrorResponse) => console.log(res)
            );
    }
}

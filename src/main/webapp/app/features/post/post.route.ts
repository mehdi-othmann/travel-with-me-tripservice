import { ActivatedRouteSnapshot, Resolve, Route, RouterStateSnapshot } from '@angular/router';
import { PostsComponent } from 'app/features/post/posts/posts.component';
import { MyPostsComponent } from 'app/features/post/my-posts/my-posts.component';
import { TripsDetailsComponent, TripsResolve } from 'app/features/trip';
import { PostsDetailsComponent } from 'app/features/post/posts-details/posts-details.component';
import { Injectable } from '@angular/core';
import { ITrip, Trip } from 'app/features/model/trip.model';
import { TripsService } from 'app/features/trip/trips.service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { PostService } from 'app/features/post/post.service';
import { IPost } from 'app/shared/model/travelwithmeproservice/post.model';
import { Post } from 'app/features/model/travelwithmeproservice/post.model';

@Injectable({ providedIn: 'root' })
export class PostsResolve implements Resolve<IPost> {
    constructor(private service: PostService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IPost> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Post>) => response.ok),
                map((post: HttpResponse<Post>) => post.body)
            );
        }
        return of(new Post());
    }
}

export const POST_ROUTE: Route[] = [
    {
        path: 'posts',
        component: PostsComponent
    },
    {
        path: 'mes-posts',
        component: MyPostsComponent,
        data: {
            authorities: ['ROLE_PRO'],
            pageTitle: 'Mes Posts'
        }
    },
    {
        path: 'posts/:id/view',
        component: PostsDetailsComponent,
        resolve: {
            post: PostsResolve
        }
    }
];

import { Component, OnInit } from '@angular/core';
import { filter, map } from 'rxjs/operators';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { IOffer } from 'app/shared/model/travelwithmeuserservice/offer.model';
import { PostService } from 'app/features/post/post.service';
import { IPost } from 'app/features/model/travelwithmeproservice/post.model';

@Component({
    selector: 'app-posts',
    templateUrl: './posts.component.html',
    styles: []
})
export class PostsComponent implements OnInit {
    posts: IPost[];

    constructor(protected postService: PostService) {}

    ngOnInit() {
        this.loadAll();
    }

    loadAll() {
        this.postService
            .query()
            .pipe(
                filter((res: HttpResponse<IPost[]>) => res.ok),
                map((res: HttpResponse<IPost[]>) => res.body)
            )
            .subscribe(
                (res: IPost[]) => {
                    this.posts = res;
                },
                (res: HttpErrorResponse) => console.log(res)
            );
    }
}

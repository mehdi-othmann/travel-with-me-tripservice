import { Component, OnInit } from '@angular/core';
import { ITrip } from 'app/features/model/trip.model';
import { IPost } from 'app/shared/model/travelwithmeproservice/post.model';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-posts-details',
    templateUrl: './posts-details.component.html',
    styles: []
})
export class PostsDetailsComponent implements OnInit {
    post: IPost;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ post }) => {
            this.post = post;
        });
    }
    previousState() {
        window.history.back();
    }
}

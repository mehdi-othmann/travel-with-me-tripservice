import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { IPost } from 'app/features/model/travelwithmeproservice/post.model';
import { createRequestOption } from 'app/shared';
import { ITrip } from 'app/features/model/trip.model';

type EntityResponseType = HttpResponse<IPost>;
type EntityArrayResponseType = HttpResponse<IPost[]>;

@Injectable({ providedIn: 'root' })
export class PostService {
    public resourceUrl = SERVER_API_URL + 'travelwithmeproservice/api/posts';

    constructor(protected http: HttpClient) {}

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IPost[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    find(id: string): Observable<EntityResponseType> {
        return this.http.get<ITrip>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}

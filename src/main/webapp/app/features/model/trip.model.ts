import { Moment } from 'moment';
import { ICategory } from 'app/features/model/category.model';
import { IUser } from 'app/core';

export interface ITrip {
    id?: string;
    title?: string;
    description?: string;
    duration?: string;
    size?: number;
    periode?: Moment;
    createdAt?: Moment;
    updatedAt?: Moment;
    image?: string;
    aproxPrice?: number;
    countryName?: string;
    countryId?: string;
    categories?: ICategory[];
    ownerLogin?: string;
    ownerId?: string;
    users?: IUser[];
    pro?: boolean;
}

export class Trip implements ITrip {
    constructor(
        public id?: string,
        public title?: string,
        public description?: string,
        public duration?: string,
        public size?: number,
        public periode?: Moment,
        public createdAt?: Moment,
        public updatedAt?: Moment,
        public aproxPrice?: number,
        public image?: string,
        public countryName?: string,
        public countryId?: string,
        public categories?: ICategory[],
        public ownerLogin?: string,
        public ownerId?: string,
        public users?: IUser[],
        public pro?: boolean
    ) {}
}

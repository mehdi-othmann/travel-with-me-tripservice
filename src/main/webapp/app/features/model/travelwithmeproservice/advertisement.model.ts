import { Moment } from 'moment';

export interface IAdvertisement {
    id?: string;
    advertisementTitle?: string;
    advertisementImage?: string;
    owner?: string;
    createdAt?: Moment;
    updatedAt?: Moment;
    expirationDate?: Moment;
}

export class Advertisement implements IAdvertisement {
    constructor(
        public id?: string,
        public advertisementTitle?: string,
        public advertisementImage?: string,
        public owner?: string,
        public createdAt?: Moment,
        public updatedAt?: Moment,
        public expirationDate?: Moment
    ) {}
}

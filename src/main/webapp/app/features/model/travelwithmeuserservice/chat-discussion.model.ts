import { IChatMessage } from 'app/shared/model/travelwithmeuserservice/chat-message.model';

export interface IChatDiscussion {
    id?: string;
    title?: string;
    chatmessages?: IChatMessage[];
}

export class ChatDiscussion implements IChatDiscussion {
    constructor(public id?: string, public title?: string, public chatmessages?: IChatMessage[]) {}
}

import { IChatDiscussion } from 'app/shared/model/travelwithmeuserservice/chat-discussion.model';

export interface IChatMessage {
    id?: string;
    content?: string;
    chatDiscussion?: IChatDiscussion;
}

export class ChatMessage implements IChatMessage {
    constructor(public id?: string, public content?: string, public chatDiscussion?: IChatDiscussion) {}
}

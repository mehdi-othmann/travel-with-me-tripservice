export interface IOffer {
    id?: string;
    name?: string;
    price?: number;
}

export class Offer implements IOffer {
    constructor(public id?: string, public name?: string, public price?: number) {}
}

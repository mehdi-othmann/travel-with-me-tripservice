export interface IFilterTrip {
    type?: string;
    size?: number;
    aproxPrice?: number;
    weeks?: string;
    country?: string;
}

export class FilterTrip implements IFilterTrip {
    constructor(public type?: string, public size?: number, public aproxPrice?: number, public weeks?: string, public country?: string) {}
}

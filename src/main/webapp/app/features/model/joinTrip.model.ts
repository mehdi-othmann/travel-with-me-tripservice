export interface IJoinTrip {
    tripId?: string;
    userLogin?: string;
}

export class JoinTrip implements IJoinTrip {
    constructor(public tripId?: string, public userLogin?: string) {}
}

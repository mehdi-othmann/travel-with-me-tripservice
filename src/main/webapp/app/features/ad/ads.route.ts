import { Route } from '@angular/router';
import { MyAdsComponent } from 'app/features/ad/my-ads/my-ads.component';

export const AD_ROUTE: Route[] = [
    {
        path: 'mes-pubs',
        component: MyAdsComponent,
        data: {
            authorities: ['ROLE_PRO'],
            pageTitle: 'Mes pubs'
        }
    }
];

import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { IPost } from 'app/features/model/travelwithmeproservice/post.model';
import { createRequestOption } from 'app/shared';
import { ITrip } from 'app/features/model/trip.model';
import { IAdvertisement } from 'app/features/model/travelwithmeproservice/advertisement.model';

type EntityArrayResponseType = HttpResponse<IPost[]>;

@Injectable({ providedIn: 'root' })
export class AdService {
    public resourceUrl = SERVER_API_URL + 'travelwithmeproservice/api/pub';

    constructor(protected http: HttpClient) {}

    getUserAds(userLogin: any, req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);

        return this.http.get<IAdvertisement[]>(`${this.resourceUrl}/user/${userLogin}`, { params: options, observe: 'response' });
    }
}

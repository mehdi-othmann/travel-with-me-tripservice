import { Component, OnInit } from '@angular/core';
import { filter, map } from 'rxjs/operators';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { IAdvertisement } from 'app/features/model/travelwithmeproservice/advertisement.model';
import { AdService } from 'app/features/ad/ad.service';
import { AccountService } from 'app/core';

@Component({
    selector: 'app-my-ads',
    templateUrl: './my-ads.component.html',
    styles: []
})
export class MyAdsComponent implements OnInit {
    advertisements: IAdvertisement[];

    constructor(protected advertisementsService: AdService, private accountService: AccountService) {}

    loadAll(login) {
        this.advertisementsService
            .getUserAds(login)
            .pipe(
                filter((res: HttpResponse<IAdvertisement[]>) => res.ok),
                map((res: HttpResponse<IAdvertisement[]>) => res.body)
            )
            .subscribe(
                (res: IAdvertisement[]) => {
                    this.advertisements = res;
                },
                (res: HttpErrorResponse) => console.log('error')
            );
    }

    ngOnInit() {
        this.accountService.identity().then(account => {
            this.loadAll(account.login);
        });
    }
}

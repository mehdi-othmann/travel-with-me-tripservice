import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AccountService, LoginModalService, LoginService } from 'app/core';
import { ProfileService } from 'app/layouts';
import { Router } from '@angular/router';
import { VERSION } from 'app/app.constants';
import { TripService } from 'app/entities/trip';
import { Observable, of } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, filter, map, switchMap, tap } from 'rxjs/operators';
import { TripsService } from 'app/features/trip/trips.service';
import { ITrip } from 'app/features/model/trip.model';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { ICountry } from 'app/features/model/country.model';
import { forEach } from '@angular/router/src/utils/collection';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styles: []
})
export class NavbarComponent implements OnInit {
    inProduction: boolean;
    isNavbarCollapsed: boolean;
    languages: any[];
    swaggerEnabled: boolean;
    modalRef: NgbModalRef;
    version: string;
    model: any;
    searchTrips: ITrip[];

    constructor(
        private loginService: LoginService,
        private accountService: AccountService,
        private loginModalService: LoginModalService,
        private profileService: ProfileService,
        private router: Router,
        private tripService: TripsService
    ) {
        this.version = VERSION ? 'v' + VERSION : '';
        this.isNavbarCollapsed = true;
    }

    search() {
        this.tripService
            .search(this.model + '*')
            .pipe(
                filter((res: HttpResponse<ITrip[]>) => res.ok),
                map((res: HttpResponse<ITrip[]>) => res.body),
                debounceTime(300)
            )
            .subscribe(
                (res: ITrip[]) => {
                    this.searchTrips = res;
                    console.log(this.searchTrips);
                },
                (res: HttpErrorResponse) => console.log('error')
            );
    }

    ngOnInit() {
        this.profileService.getProfileInfo().then(profileInfo => {
            this.inProduction = profileInfo.inProduction;
            this.swaggerEnabled = profileInfo.swaggerEnabled;
        });
    }

    collapseNavbar() {
        this.isNavbarCollapsed = true;
    }

    isAuthenticated() {
        return this.accountService.isAuthenticated();
    }

    login() {
        this.modalRef = this.loginModalService.open();
    }

    logout() {
        this.collapseNavbar();
        this.loginService.logout();
        this.router.navigate(['']);
    }

    toggleNavbar() {
        this.isNavbarCollapsed = !this.isNavbarCollapsed;
    }

    getImageUrl() {
        return this.isAuthenticated() ? this.accountService.getImageUrl() : null;
    }

    redirectToTrip() {
        for (let trip of this.searchTrips) {
            if (trip.title === this.model) {
                this.router.navigate(['/voyages/' + trip.id + '/view']);
            }
        }
    }
}

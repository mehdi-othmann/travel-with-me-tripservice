import { Routes } from '@angular/router';
import { NavbarComponent } from 'app/features/layouts/navbar/navbar.component';

export const appNavbarRoute: Routes = [
    {
        path: '',
        component: NavbarComponent,
        outlet: 'app-navbar'
    }
];

import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { OffersComponent } from 'app/features/offer/offers/offers.component';
import { OFFERS_ROUTE } from 'app/features/offer/offers.route';
import { TravelWithMeTripServiceSharedModule } from 'app/shared';
import { EditTripComponent, MyTripsComponent, TRIPS_ROUTE, TripsComponent, TripsDetailsComponent } from 'app/features/trip';
import { PostsComponent } from './post/posts/posts.component';
import { POST_ROUTE } from 'app/features/post/post.route';
import { MyPostsComponent } from './post/my-posts/my-posts.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { NewTripComponent } from './trip/new-trip/new-trip.component';
import { FilterTripsComponent } from './trip/filter-trips/filter-trips.component';
import { MyAdsComponent } from './ad/my-ads/my-ads.component';
import { AD_ROUTE } from 'app/features/ad/ads.route';
import { MyConvComponent } from './conv/my-conv/my-conv.component';
import { CONV_ROUTE } from 'app/features/conv/conv.route';
import { ConvDetailsComponent } from './conv/conv-details/conv-details.component';
import { TripCardComponent } from './trip/trip-card/trip-card.component';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { UserCardComponent } from './user/user-card/user-card.component';
import { UsersComponent } from './user/users/users.component';
import { USERS_ROUTE } from 'app/features/user/users.route';
import { PostsDetailsComponent } from './post/posts-details/posts-details.component';

@NgModule({
    declarations: [
        OffersComponent,
        TripsComponent,
        MyTripsComponent,
        TripsDetailsComponent,
        EditTripComponent,
        PostsComponent,
        MyPostsComponent,
        FooterComponent,
        NavbarComponent,
        NewTripComponent,
        FilterTripsComponent,
        MyAdsComponent,
        MyConvComponent,
        ConvDetailsComponent,
        TripCardComponent,
        UserCardComponent,
        UsersComponent,
        PostsDetailsComponent
    ],
    imports: [
        TravelWithMeTripServiceSharedModule,
        CommonModule,
        RouterModule.forChild([...OFFERS_ROUTE, ...TRIPS_ROUTE, ...POST_ROUTE, ...AD_ROUTE, ...CONV_ROUTE, ...USERS_ROUTE]),
        ScrollToModule
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    exports: [FooterComponent, NavbarComponent],
    providers: []
})
export class FeaturesModule {}

package fr.univparis8.iut.tripservice.service;

import com.github.javafaker.Faker;
import fr.univparis8.iut.tripservice.domain.*;
import fr.univparis8.iut.tripservice.repository.CategoryRepository;
import fr.univparis8.iut.tripservice.repository.CountryRepository;
import fr.univparis8.iut.tripservice.repository.TripsRepository;
import fr.univparis8.iut.tripservice.repository.UserRepository;
import fr.univparis8.iut.tripservice.repository.search.TripsSearchRepository;
import fr.univparis8.iut.tripservice.security.AuthoritiesConstants;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.*;

@Service
public class GeneratorService {

    private final UserRepository userRepository;

    private final CategoryRepository categoryRepository;

    private final MongoTemplate mongoTemplate;

    private final CountryRepository countryRepository;

    private final TripsSearchRepository tripsSearchRepository;
    
    private final TripsRepository tripsRepository;

    public GeneratorService(UserRepository userRepository, CategoryRepository categoryRepository, MongoTemplate mongoTemplate, CountryRepository countryRepository, TripsSearchRepository tripsSearchRepository, TripsRepository tripsRepository) {
        this.userRepository = userRepository;
        this.categoryRepository = categoryRepository;
        this.mongoTemplate = mongoTemplate;
        this.countryRepository = countryRepository;
        this.tripsSearchRepository = tripsSearchRepository;
        this.tripsRepository = tripsRepository;
    }

    public void generateProTrip(int amount) {
        generateTrip(amount, true);
    }


    public void generateClientTrip(int amount) {
        generateTrip(amount, false);
    }

    public void generateCountry(int amout) {
        Faker faker = new Faker(new Locale("fr"));
        for (int i = 0; i < amout; i++) {
            Country country = new Country();
            country.setName(faker.address().country());
            mongoTemplate.save(country);
        }
    }

    private void generateTrip(int amount, boolean isPro) {
        Faker faker = new Faker();

        Authority clientAuthority = new Authority();
        clientAuthority.setName(AuthoritiesConstants.CLIENT);
        Authority proAuthority = new Authority();
        proAuthority.setName(AuthoritiesConstants.PRO);


        List<User> proUsers = userRepository.findAllByAuthoritiesEquals(proAuthority);
        List<User> clientUsers = userRepository.findAllByAuthoritiesEquals(clientAuthority);
        List<Country> countries = countryRepository.findAll();
        List<Category> categories = categoryRepository.findAll();

        double minPrice = 1000;
        double maxPrice = 6000;

        Random r = new Random();

        LocalDate now = LocalDate.now();

        for (int j = 0; j < amount; j++) {
            double randomValue = minPrice + (maxPrice - minPrice) * r.nextDouble();
            int size = r.nextInt(9) + 2;

            int randomClient = r.nextInt(clientUsers.size());
            int randomPro = r.nextInt(proUsers.size());
            int randomCountry = r.nextInt(countries.size());


            Trip trip = new Trip();
            trip.setImage(faker.internet().image());
            trip.setTitle(faker.lorem().sentence());
            trip.setDescription(faker.lorem().paragraph());
            trip.setAproxPrice(BigDecimal.valueOf(randomValue)
                .setScale(2, RoundingMode.HALF_UP)
                .doubleValue());
            trip.setCreatedAt(now);
            trip.updatedAt(now.plusDays(3));
            trip.setPeriode(now.plusDays(13));
            trip.setSize(size);
            trip.setDuration(size + " semaines");
            for (int k = 0; k < 3; k++) {
                int randomCategory = r.nextInt(categories.size());
                trip.addCategories(categories.get(randomCategory));
            }
            trip.setCountry(countries.get(randomCountry));
            if (isPro) {
                trip.setOwner(proUsers.get(randomPro));
            } else {
                trip.setOwner(clientUsers.get(randomClient));
            }

            for (int k = 0; k < 3; k++) {
                randomClient = r.nextInt(clientUsers.size());
                trip.addUsers(clientUsers.get(randomClient));
            }

            mongoTemplate.save(trip);
        }


    }

    public void syncElasticTrip() {
        tripsSearchRepository.deleteAll();
        List<Trip> trips = tripsRepository.findAll();
        tripsSearchRepository.saveAll(trips);
    }

    public List<String> getProUsers() {
        Authority authority = new Authority();
        authority.setName("ROLE_PRO");

        List<User> users = userRepository.findAllByAuthoritiesEquals(authority);

        List<String> usersId = new ArrayList<>();

        for (User user : users) {
            usersId.add(user.getId());
        }

        return usersId;
    }
}

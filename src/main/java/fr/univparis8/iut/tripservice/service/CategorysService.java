package fr.univparis8.iut.tripservice.service;

import fr.univparis8.iut.tripservice.domain.Category;
import fr.univparis8.iut.tripservice.repository.CategoryRepository;
import fr.univparis8.iut.tripservice.repository.CategorysRepository;
import fr.univparis8.iut.tripservice.repository.search.CategorySearchRepository;
import fr.univparis8.iut.tripservice.service.dto.CategoryDTO;
import fr.univparis8.iut.tripservice.service.dto.CategorysDTO;
import fr.univparis8.iut.tripservice.service.mapper.CategoryMapper;
import fr.univparis8.iut.tripservice.service.mapper.CategorysMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing Category.
 */
@Service
public class CategorysService {

    private final CategorysRepository categorysRepository;

    private final CategorysMapper categorysMapper;

    public CategorysService(CategorysRepository categorysRepository, CategorysMapper categorysMapper) {
        this.categorysRepository = categorysRepository;
        this.categorysMapper = categorysMapper;
    }


    public List<CategorysDTO> findHomeCategory() {
        return categorysRepository.findFirst9By().stream()
            .map(categorysMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }
}

package fr.univparis8.iut.tripservice.service.mapper;

import fr.univparis8.iut.tripservice.domain.Country;
import fr.univparis8.iut.tripservice.service.dto.CountryDTO;
import fr.univparis8.iut.tripservice.service.dto.CountrysDTO;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity Country and its DTO CountryDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CountrysMapper extends EntityMapper<CountrysDTO, Country> {



    default Country fromId(String id) {
        if (id == null) {
            return null;
        }
        Country country = new Country();
        country.setId(id);
        return country;
    }
}

package fr.univparis8.iut.tripservice.service;

import fr.univparis8.iut.tripservice.domain.UserInfo;
import fr.univparis8.iut.tripservice.repository.UserInfoRepository;
import fr.univparis8.iut.tripservice.repository.UserInfosRepository;
import fr.univparis8.iut.tripservice.repository.search.UserInfoSearchRepository;
import fr.univparis8.iut.tripservice.service.dto.UserInfoDTO;
import fr.univparis8.iut.tripservice.service.dto.UserInfosDTO;
import fr.univparis8.iut.tripservice.service.mapper.UserInfoMapper;
import fr.univparis8.iut.tripservice.service.mapper.UserInfosMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing UserInfo.
 */
@Service
public class UserInfosService {

    private final Logger log = LoggerFactory.getLogger(UserInfosService.class);

    private final UserInfosRepository userInfoRepository;

    private final UserInfosMapper userInfoMapper;

    public UserInfosService(UserInfosRepository userInfoRepository, UserInfosMapper userInfoMapper, UserInfoSearchRepository userInfoSearchRepository) {
        this.userInfoRepository = userInfoRepository;
        this.userInfoMapper = userInfoMapper;
    }

    public Page<UserInfosDTO> findAll(Pageable pageable) {
        return userInfoRepository.findAll(pageable)
            .map(userInfoMapper::toDto);
    }
}

package fr.univparis8.iut.tripservice.service.dto;

public class FilterTripDTO {
    private String type;
    private int size;
    private int aproxPrice;
    private String weeks;
    private String country;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getAproxPrice() {
        return aproxPrice;
    }

    public void setAproxPrice(int aproxPrice) {
        this.aproxPrice = aproxPrice;
    }

    public String getWeeks() {
        return weeks;
    }

    public void setWeeks(String weeks) {
        this.weeks = weeks;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}

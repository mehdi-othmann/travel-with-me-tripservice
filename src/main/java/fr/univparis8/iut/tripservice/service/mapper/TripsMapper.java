package fr.univparis8.iut.tripservice.service.mapper;

import fr.univparis8.iut.tripservice.domain.Trip;
import fr.univparis8.iut.tripservice.service.dto.TripsDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {CountryMapper.class, CategoryMapper.class, UserMapper.class})
public interface TripsMapper extends EntityMapper<TripsDTO, Trip> {

    @Mapping(source = "country.id", target = "countryId")
    @Mapping(source = "country.name", target = "countryName")
    @Mapping(source = "owner.id", target = "ownerId")
    @Mapping(source = "owner.login", target = "ownerLogin")
    TripsDTO toDto(Trip trip);

    @Mapping(source = "countryId", target = "country")
    @Mapping(source = "ownerId", target = "owner")
    Trip toEntity(TripsDTO tripDTO);

    default Trip fromId(String id) {
        if (id == null) {
            return null;
        }
        Trip trip = new Trip();
        trip.setId(id);
        return trip;
    }
}

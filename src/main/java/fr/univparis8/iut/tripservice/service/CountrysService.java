package fr.univparis8.iut.tripservice.service;

import fr.univparis8.iut.tripservice.repository.CountrysRepository;
import fr.univparis8.iut.tripservice.service.dto.CountrysDTO;
import fr.univparis8.iut.tripservice.service.mapper.CountrysMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Country.
 */
@Service
public class CountrysService {

    private final Logger log = LoggerFactory.getLogger(CountrysService.class);

    private final CountrysRepository countryRepository;

    private final CountrysMapper countryMapper;


    public CountrysService(CountrysRepository countryRepository, CountrysMapper countryMapper) {
        this.countryRepository = countryRepository;
        this.countryMapper = countryMapper;
    }

    public List<CountrysDTO> findAll() {
        return countryRepository.findAll().stream()
            .map(countryMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

}

package fr.univparis8.iut.tripservice.service.mapper;

import fr.univparis8.iut.tripservice.domain.Category;
import fr.univparis8.iut.tripservice.service.dto.CategoryDTO;
import fr.univparis8.iut.tripservice.service.dto.CategorysDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity Category and its DTO CategoryDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CategorysMapper extends EntityMapper<CategorysDTO, Category> {


    @Mapping(target = "trips", ignore = true)
    Category toEntity(CategoryDTO categoryDTO);

    default Category fromId(String id) {
        if (id == null) {
            return null;
        }
        Category category = new Category();
        category.setId(id);
        return category;
    }
}

package fr.univparis8.iut.tripservice.service.mapper;

import fr.univparis8.iut.tripservice.domain.*;
import fr.univparis8.iut.tripservice.service.dto.TripDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Trip and its DTO TripDTO.
 */
@Mapper(componentModel = "spring", uses = {CountryMapper.class, CategoryMapper.class, UserMapper.class})
public interface TripMapper extends EntityMapper<TripDTO, Trip> {

    @Mapping(source = "country.id", target = "countryId")
    @Mapping(source = "country.name", target = "countryName")
    @Mapping(source = "owner.id", target = "ownerId")
    @Mapping(source = "owner.login", target = "ownerLogin")
    TripDTO toDto(Trip trip);

    @Mapping(source = "countryId", target = "country")
    @Mapping(source = "ownerId", target = "owner")
    Trip toEntity(TripDTO tripDTO);

    default Trip fromId(String id) {
        if (id == null) {
            return null;
        }
        Trip trip = new Trip();
        trip.setId(id);
        return trip;
    }
}

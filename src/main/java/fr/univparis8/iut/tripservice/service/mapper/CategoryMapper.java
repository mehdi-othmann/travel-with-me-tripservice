package fr.univparis8.iut.tripservice.service.mapper;

import fr.univparis8.iut.tripservice.domain.*;
import fr.univparis8.iut.tripservice.service.dto.CategoryDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Category and its DTO CategoryDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CategoryMapper extends EntityMapper<CategoryDTO, Category> {


    @Mapping(target = "trips", ignore = true)
    Category toEntity(CategoryDTO categoryDTO);

    default Category fromId(String id) {
        if (id == null) {
            return null;
        }
        Category category = new Category();
        category.setId(id);
        return category;
    }
}

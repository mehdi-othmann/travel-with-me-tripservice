package fr.univparis8.iut.tripservice.service.dto;
import java.io.Serializable;
import java.util.Objects;

public class JoinTripDTO implements Serializable {

    private String tripId;

    private String userLogin;

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JoinTripDTO that = (JoinTripDTO) o;
        return Objects.equals(tripId, that.tripId) &&
            Objects.equals(userLogin, that.userLogin);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tripId, userLogin);
    }

    @Override
    public String toString() {
        return "JoinTripDTO{" +
            "tripId='" + tripId + '\'' +
            ", userLogin='" + userLogin + '\'' +
            '}';
    }
}

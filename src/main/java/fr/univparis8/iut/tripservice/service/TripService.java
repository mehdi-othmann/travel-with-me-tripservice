package fr.univparis8.iut.tripservice.service;

import fr.univparis8.iut.tripservice.domain.Trip;
import fr.univparis8.iut.tripservice.repository.TripRepository;
import fr.univparis8.iut.tripservice.repository.search.TripSearchRepository;
import fr.univparis8.iut.tripservice.service.dto.TripDTO;
import fr.univparis8.iut.tripservice.service.mapper.TripMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Trip.
 */
@Service
public class TripService {

    private final Logger log = LoggerFactory.getLogger(TripService.class);

    private final TripRepository tripRepository;

    private final TripMapper tripMapper;

    private final TripSearchRepository tripSearchRepository;

    public TripService(TripRepository tripRepository, TripMapper tripMapper, TripSearchRepository tripSearchRepository) {
        this.tripRepository = tripRepository;
        this.tripMapper = tripMapper;
        this.tripSearchRepository = tripSearchRepository;
    }

    /**
     * Save a trip.
     *
     * @param tripDTO the entity to save
     * @return the persisted entity
     */
    public TripDTO save(TripDTO tripDTO) {
        log.debug("Request to save Trip : {}", tripDTO);
        Trip trip = tripMapper.toEntity(tripDTO);
        trip = tripRepository.save(trip);
        TripDTO result = tripMapper.toDto(trip);
        tripSearchRepository.save(trip);
        return result;
    }

    /**
     * Get all the trips.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    public Page<TripDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Trips");
        return tripRepository.findAll(pageable)
            .map(tripMapper::toDto);
    }

    /**
     * Get all the Trip with eager load of many-to-many relationships.
     *
     * @return the list of entities
     */
    public Page<TripDTO> findAllWithEagerRelationships(Pageable pageable) {
        return tripRepository.findAllWithEagerRelationships(pageable).map(tripMapper::toDto);
    }
    

    /**
     * Get one trip by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    public Optional<TripDTO> findOne(String id) {
        log.debug("Request to get Trip : {}", id);
        return tripRepository.findOneWithEagerRelationships(id)
            .map(tripMapper::toDto);
    }

    /**
     * Delete the trip by id.
     *
     * @param id the id of the entity
     */
    public void delete(String id) {
        log.debug("Request to delete Trip : {}", id);
        tripRepository.deleteById(id);
        tripSearchRepository.deleteById(id);
    }

    /**
     * Search for the trip corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    public Page<TripDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Trips for query {}", query);
        return tripSearchRepository.search(queryStringQuery(query), pageable)
            .map(tripMapper::toDto);
    }
}

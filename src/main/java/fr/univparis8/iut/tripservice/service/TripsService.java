package fr.univparis8.iut.tripservice.service;

import fr.univparis8.iut.tripservice.domain.Authority;
import fr.univparis8.iut.tripservice.domain.Trip;
import fr.univparis8.iut.tripservice.domain.User;
import fr.univparis8.iut.tripservice.repository.AuthorityRepository;
import fr.univparis8.iut.tripservice.repository.TripsRepository;
import fr.univparis8.iut.tripservice.repository.UserRepository;
import fr.univparis8.iut.tripservice.repository.search.TripsSearchRepository;
import fr.univparis8.iut.tripservice.service.dto.FilterTripDTO;
import fr.univparis8.iut.tripservice.service.dto.JoinTripDTO;
import fr.univparis8.iut.tripservice.service.dto.TripsDTO;
import fr.univparis8.iut.tripservice.service.mapper.TripsMapper;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

@Service
public class TripsService {

    private final Logger log = LoggerFactory.getLogger(TripsService.class);

    private final TripsRepository tripsRepository;

    private final TripsMapper tripsMapper;

    private final UserRepository userRepository;

    private final AuthorityRepository authorityRepository;

    private final MongoTemplate mongoTemplate;

    private final TripsSearchRepository tripsSearchRepository;

    public TripsService(TripsRepository tripsRepository,
                        TripsMapper tripsMapper,
                        UserRepository userRepository,
                        AuthorityRepository authorityRepository,
                        MongoTemplate mongoTemplate, TripsSearchRepository tripsSearchRepository) {
        this.tripsRepository = tripsRepository;
        this.tripsMapper = tripsMapper;
        this.userRepository = userRepository;
        this.authorityRepository = authorityRepository;
        this.mongoTemplate = mongoTemplate;
        this.tripsSearchRepository = tripsSearchRepository;
    }

    public Optional<TripsDTO> findOne(String id) {
        return tripsRepository.findOneWithEagerRelationships(id)
            .map(tripsMapper::toDto);
    }

    public List<TripsDTO> findUserTrips(String userId) {
        if (userId.startsWith("5")) {
            ObjectId userIdd = new ObjectId(userId);
            return tripsRepository.findUserAllWithEagerRelationshipsId(userIdd, userId).stream()
                .map(tripsMapper::toDto)
                .collect(Collectors.toCollection(LinkedList::new));
        }

        return tripsRepository.findUserAllWithEagerRelationships(userId).stream()
            .map(tripsMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    public void addUserToTrip(JoinTripDTO joinTripDTO) {
        userRepository.findOneByLogin(joinTripDTO.getUserLogin()).ifPresent(
            existingUser -> {
                tripsRepository.findById(joinTripDTO.getTripId()).ifPresent(
                    existingTrip -> {
                        existingTrip.addUsers(existingUser);
                        tripsRepository.save(existingTrip);
                    }
                );
            }
        );
    }

    public Page<TripsDTO> findAll(Pageable pageable) {
        Page<TripsDTO> trips = tripsRepository.findAllByOrderByCreatedAtDesc(pageable)
            .map(tripsMapper::toDto);


        trips.forEach(
            trip -> userRepository.findOneByLogin(trip.getOwnerLogin()).ifPresent(
                owner -> {
                    for (Authority authorithy : owner.getAuthorities()) {
                        if (authorithy.getName().equals("ROLE_PRO")) {
                            trip.setPro(true);
                        }
                    }
                }
            )

        );

        return trips;
    }

    public Page<TripsDTO> findAllWithEagerRelationships(Pageable pageable) {
        return tripsRepository.findAllWithEagerRelationships(pageable).map(tripsMapper::toDto);
    }

    public Page<TripsDTO> findAllFilter(Pageable pageable, FilterTripDTO filterTripDTO) {

        Query query = new Query();

        if (!filterTripDTO.getType().isEmpty()) {

            Optional<Authority> authority = authorityRepository.findAuthorityByNameEquals(filterTripDTO.getType());

            List<User> users = userRepository.findAllByAuthoritiesEquals(authority.get());

            List<String> usersId = new ArrayList<>();

            for (User user : users) {
                usersId.add(user.getId());
            }

            query.addCriteria(Criteria.where("ownerId").in(usersId));
        }

        if (0 != filterTripDTO.getSize()) {
            if (filterTripDTO.getSize() == 5) {
                query.addCriteria(Criteria.where("size").gte(filterTripDTO.getSize()));
            } else {
                query.addCriteria(Criteria.where("size").is(filterTripDTO.getSize()));
            }
        }


        if (0 != filterTripDTO.getAproxPrice()) {
            switch (filterTripDTO.getAproxPrice()) {
                case 1000:
                    query.addCriteria(Criteria.where("aproxPrice").lte(1000));
                    break;
                case 2000:
                    query.addCriteria(Criteria.where("aproxPrice").lte(2000)
                        .orOperator(Criteria.where("aproxPrice").gte(1000)));
                    break;
                case 3000:
                    query.addCriteria(Criteria.where("aproxPrice").lte(3000)
                        .orOperator(Criteria.where("aproxPrice").gte(2000)));

                    break;
                case 4000:
                    query.addCriteria(Criteria.where("aproxPrice").lte(5000)
                        .orOperator(Criteria.where("aproxPrice").gte(4000)));
                    break;
                case 5000:
                    query.addCriteria(Criteria.where("aproxPrice").gte(5000));
                    break;
            }

        }

        if (!filterTripDTO.getWeeks().equals("")) {
            query.addCriteria(Criteria.where("duration").is(filterTripDTO.getWeeks()));
        }

        if (!filterTripDTO.getCountry().equals("")) {
            query.addCriteria(Criteria.where("countryId").is(filterTripDTO.getCountry()));
        }


        List<Trip> tripsByFilter = mongoTemplate.find(query, Trip.class);


        List<TripsDTO> tripsDTOS = tripsByFilter.stream()
            .map(tripsMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));


        Page<TripsDTO> trips = new PageImpl<>(tripsDTOS, pageable, tripsDTOS.size());

        //todo refactor to method
        trips.forEach(
            trip -> userRepository.findOneByLogin(trip.getOwnerLogin()).ifPresent(
                owner -> {
                    for (Authority authorithy : owner.getAuthorities()) {
                        if (authorithy.getName().equals("ROLE_PRO")) {
                            trip.setPro(true);
                        }
                    }
                }
            )
        );


        return trips;
    }

    public TripsDTO save(TripsDTO tripsDTO) {
        Trip trip = tripsMapper.toEntity(tripsDTO);
        trip = tripsRepository.save(trip);
        TripsDTO result = tripsMapper.toDto(trip);

        return result;
    }

    public Page<TripsDTO> search(String query, Pageable pageable) {
        return tripsSearchRepository.search(queryStringQuery(query), pageable)
            .map(tripsMapper::toDto);
    }
}

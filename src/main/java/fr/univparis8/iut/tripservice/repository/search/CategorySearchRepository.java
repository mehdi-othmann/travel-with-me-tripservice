package fr.univparis8.iut.tripservice.repository.search;

import fr.univparis8.iut.tripservice.domain.Category;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Category entity.
 */
public interface CategorySearchRepository extends ElasticsearchRepository<Category, String> {
}

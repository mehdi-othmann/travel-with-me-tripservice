package fr.univparis8.iut.tripservice.repository;

import fr.univparis8.iut.tripservice.domain.Trip;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data MongoDB repository for the Trip entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TripRepository extends MongoRepository<Trip, String> {
    @Query("{}")
    Page<Trip> findAllWithEagerRelationships(Pageable pageable);

    @Query("{}")
    List<Trip> findAllWithEagerRelationships();

    @Query("{'id': ?0}")
    Optional<Trip> findOneWithEagerRelationships(String id);

}

package fr.univparis8.iut.tripservice.repository.search;

import fr.univparis8.iut.tripservice.domain.Trip;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Trip entity.
 */
public interface TripSearchRepository extends ElasticsearchRepository<Trip, String> {
}

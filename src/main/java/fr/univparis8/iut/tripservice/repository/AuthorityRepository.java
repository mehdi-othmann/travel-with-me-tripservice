package fr.univparis8.iut.tripservice.repository;

import fr.univparis8.iut.tripservice.domain.Authority;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

/**
 * Spring Data MongoDB repository for the Authority entity.
 */
public interface AuthorityRepository extends MongoRepository<Authority, String> {

    Optional<Authority> findAuthorityByNameEquals(String name);
}

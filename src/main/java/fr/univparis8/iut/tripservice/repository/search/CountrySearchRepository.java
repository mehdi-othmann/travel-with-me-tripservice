package fr.univparis8.iut.tripservice.repository.search;

import fr.univparis8.iut.tripservice.domain.Country;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Country entity.
 */
public interface CountrySearchRepository extends ElasticsearchRepository<Country, String> {
}

package fr.univparis8.iut.tripservice.repository;

import fr.univparis8.iut.tripservice.domain.Trip;
import fr.univparis8.iut.tripservice.domain.User;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.LongStream;

/**
 * Spring Data MongoDB repository for the Trip entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TripsRepository extends MongoRepository<Trip, String> {
    @Query("{}")
    Page<Trip> findAllWithEagerRelationships(Pageable pageable);

    @Query("{'$or':[ {'users.$id': ?0} , {'ownerId': ?0} ] }")
    List<Trip> findUserAllWithEagerRelationships(String userId);

    @Query("{'$or':[ {'users.$id': ?0} , {'ownerId': ?1} ] }")
    List<Trip> findUserAllWithEagerRelationshipsId(ObjectId userId, String userI);

    @Query("{'id': ?0}")
    Optional<Trip> findOneWithEagerRelationships(String id);

    List<Trip> findTripsByOwnerIsIn(List<User> user);

    Page <Trip> findAllByOrderByCreatedAtDesc(Pageable var1);
}

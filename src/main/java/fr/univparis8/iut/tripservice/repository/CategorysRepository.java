package fr.univparis8.iut.tripservice.repository;

import fr.univparis8.iut.tripservice.domain.Category;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data MongoDB repository for the Category entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CategorysRepository extends MongoRepository<Category, String> {

    List<Category> findFirst9By();
}

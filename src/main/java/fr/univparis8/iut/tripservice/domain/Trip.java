package fr.univparis8.iut.tripservice.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Trip.
 */
@Document(collection = "trip")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "trip")
public class Trip implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    private String id;

    @Field("title")
    private String title;

    @Field("description")
    private String description;

    @Field("duration")
    private String duration;

    @Field("size")
    private Integer size;

    @Field("periode")
    private LocalDate periode;

    @Field("created_at")
    private LocalDate createdAt;

    @Field("updated_at")
    private LocalDate updatedAt;

    @Field("aprox_price")
    private Double aproxPrice;

    @Field("image")
    private String image;

    @DBRef
    @Field("country")
    private Country country;

    @DBRef
    @Field("categories")
    private Set<Category> categories = new HashSet<>();

    @DBRef
    @Field("owner")
    @JsonIgnoreProperties("trips")
    private User owner;

    @DBRef
    @Field("users")
    private Set<User> users = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public Trip title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public Trip description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDuration() {
        return duration;
    }

    public Trip duration(String duration) {
        this.duration = duration;
        return this;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public Integer getSize() {
        return size;
    }

    public Trip size(Integer size) {
        this.size = size;
        return this;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public LocalDate getPeriode() {
        return periode;
    }

    public Trip periode(LocalDate periode) {
        this.periode = periode;
        return this;
    }

    public void setPeriode(LocalDate periode) {
        this.periode = periode;
    }

    public LocalDate getCreatedAt() {
        return createdAt;
    }

    public Trip createdAt(LocalDate createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDate getUpdatedAt() {
        return updatedAt;
    }

    public Trip updatedAt(LocalDate updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public void setUpdatedAt(LocalDate updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Double getAproxPrice() {
        return aproxPrice;
    }

    public Trip aproxPrice(Double aproxPrice) {
        this.aproxPrice = aproxPrice;
        return this;
    }

    public void setAproxPrice(Double aproxPrice) {
        this.aproxPrice = aproxPrice;
    }

    public String getImage() {
        return image;
    }

    public Trip image(String image) {
        this.image = image;
        return this;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Country getCountry() {
        return country;
    }

    public Trip country(Country country) {
        this.country = country;
        return this;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Set<Category> getCategories() {
        return categories;
    }

    public Trip categories(Set<Category> categories) {
        this.categories = categories;
        return this;
    }

    public Trip addCategories(Category category) {
        this.categories.add(category);
        category.getTrips().add(this);
        return this;
    }

    public Trip removeCategories(Category category) {
        this.categories.remove(category);
        category.getTrips().remove(this);
        return this;
    }

    public void setCategories(Set<Category> categories) {
        this.categories = categories;
    }

    public User getOwner() {
        return owner;
    }

    public Trip owner(User user) {
        this.owner = user;
        return this;
    }

    public void setOwner(User user) {
        this.owner = user;
    }

    public Set<User> getUsers() {
        return users;
    }

    public Trip users(Set<User> users) {
        this.users = users;
        return this;
    }

    public Trip addUsers(User user) {
        this.users.add(user);
        return this;
    }

    public Trip removeUsers(User user) {
        this.users.remove(user);
        return this;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Trip trip = (Trip) o;
        if (trip.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), trip.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Trip{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", description='" + getDescription() + "'" +
            ", duration='" + getDuration() + "'" +
            ", size=" + getSize() +
            ", periode='" + getPeriode() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", aproxPrice=" + getAproxPrice() +
            ", image='" + getImage() + "'" +
            "}";
    }
}

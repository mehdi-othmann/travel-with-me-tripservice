package fr.univparis8.iut.tripservice.domain;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A UserInfo.
 */
@Document(collection = "user_info")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "userinfo")
public class UserInfo implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    private String id;

    @Field("description")
    private String description;

    @Field("birth_day")
    private LocalDate birthDay;

    @Field("job")
    private String job;

    @Field("favorite_artist")
    private String favoriteArtist;

    @Field("favorite_book")
    private String favoriteBook;

    @DBRef
    @Field("user")
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public UserInfo description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getBirthDay() {
        return birthDay;
    }

    public UserInfo birthDay(LocalDate birthDay) {
        this.birthDay = birthDay;
        return this;
    }

    public void setBirthDay(LocalDate birthDay) {
        this.birthDay = birthDay;
    }

    public String getJob() {
        return job;
    }

    public UserInfo job(String job) {
        this.job = job;
        return this;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getFavoriteArtist() {
        return favoriteArtist;
    }

    public UserInfo favoriteArtist(String favoriteArtist) {
        this.favoriteArtist = favoriteArtist;
        return this;
    }

    public void setFavoriteArtist(String favoriteArtist) {
        this.favoriteArtist = favoriteArtist;
    }

    public String getFavoriteBook() {
        return favoriteBook;
    }

    public UserInfo favoriteBook(String favoriteBook) {
        this.favoriteBook = favoriteBook;
        return this;
    }

    public void setFavoriteBook(String favoriteBook) {
        this.favoriteBook = favoriteBook;
    }

    public User getUser() {
        return user;
    }

    public UserInfo user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserInfo userInfo = (UserInfo) o;
        if (userInfo.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userInfo.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserInfo{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            ", birthDay='" + getBirthDay() + "'" +
            ", job='" + getJob() + "'" +
            ", favoriteArtist='" + getFavoriteArtist() + "'" +
            ", favoriteBook='" + getFavoriteBook() + "'" +
            "}";
    }
}

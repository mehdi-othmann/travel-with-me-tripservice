package fr.univparis8.iut.tripservice.web.rest;

import fr.univparis8.iut.tripservice.service.CountrysService;
import fr.univparis8.iut.tripservice.service.dto.CountryDTO;
import fr.univparis8.iut.tripservice.service.dto.CountrysDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * REST controller for managing Country.
 */
@RestController
@RequestMapping("/api")
public class CountrysResource {

    private final Logger log = LoggerFactory.getLogger(CountrysResource.class);

    private static final String ENTITY_NAME = "country";

    private final CountrysService countryService;

    public CountrysResource(CountrysService countryService) {
        this.countryService = countryService;
    }


    @GetMapping("/countrie")
    public List<CountrysDTO> getAllCountries() {
        return countryService.findAll();
    }

}

package fr.univparis8.iut.tripservice.web.rest;

import fr.univparis8.iut.tripservice.service.GeneratorService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class GeneratorRessource {

    private final GeneratorService generatorService;

    public GeneratorRessource(GeneratorService generatorService) {
        this.generatorService = generatorService;
    }


    @GetMapping("/generate/country/{amount}")
    public void GenerateCountry(@PathVariable int amount) {
        generatorService.generateCountry(amount);
    }

    @GetMapping("/generate/clientTrip/{amount}")
    public void GenerateClientTrip(@PathVariable int amount) {
        generatorService.generateClientTrip(amount);
    }

    @GetMapping("/generate/proTrip/{amount}")
    public void GenerateProTrip(@PathVariable int amount) {
        generatorService.generateProTrip(amount);
    }

    @GetMapping("/sync/elastic/trips")
    public void GenerateProTrip() {
        generatorService.syncElasticTrip();
    }

    @GetMapping("/proUsersId")
    public List<String> GetAllProUserId() {
        return generatorService.getProUsers();
    }
}

package fr.univparis8.iut.tripservice.web.rest;

import fr.univparis8.iut.tripservice.service.CategoryService;
import fr.univparis8.iut.tripservice.service.CategorysService;
import fr.univparis8.iut.tripservice.service.dto.CategoryDTO;
import fr.univparis8.iut.tripservice.service.dto.CategorysDTO;
import fr.univparis8.iut.tripservice.web.rest.errors.BadRequestAlertException;
import fr.univparis8.iut.tripservice.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Category.
 */
@RestController
@RequestMapping("/api")
public class CategorysResource {

    private final CategorysService categorysService;

    public CategorysResource(CategorysService categorysService) {
        this.categorysService = categorysService;
    }


    @GetMapping("/category/home")
    public List<CategorysDTO> getHomeCategory() {
        return categorysService.findHomeCategory();
    }


}

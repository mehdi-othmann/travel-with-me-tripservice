package fr.univparis8.iut.tripservice.web.rest;

import fr.univparis8.iut.tripservice.service.UserInfoService;
import fr.univparis8.iut.tripservice.service.UserInfosService;
import fr.univparis8.iut.tripservice.service.dto.UserInfoDTO;
import fr.univparis8.iut.tripservice.service.dto.UserInfosDTO;
import fr.univparis8.iut.tripservice.web.rest.errors.BadRequestAlertException;
import fr.univparis8.iut.tripservice.web.rest.util.HeaderUtil;
import fr.univparis8.iut.tripservice.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing UserInfo.
 */
@RestController
@RequestMapping("/api")
public class UserInfosResource {

    private final Logger log = LoggerFactory.getLogger(UserInfosResource.class);

    private static final String ENTITY_NAME = "userInfo";

    private final UserInfosService userInfoService;

    public UserInfosResource(UserInfosService userInfoService) {
        this.userInfoService = userInfoService;
    }


    @GetMapping("/user-info")
    public ResponseEntity<List<UserInfosDTO>> getAllUserInfos(Pageable pageable) {
        Page<UserInfosDTO> page = userInfoService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/user-infos");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}

/**
 * View Models used by Spring MVC REST controllers.
 */
package fr.univparis8.iut.tripservice.web.rest.vm;

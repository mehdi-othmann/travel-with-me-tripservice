package fr.univparis8.iut.tripservice.web.rest;

import fr.univparis8.iut.tripservice.service.dto.userservice.ChatDiscussionsDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.Optional;

@FeignClient("travelwithmeuserservice")
public interface UserServiceClient {

    @RequestMapping(method = RequestMethod.GET, value = "/api/chat-discussions/trip/{id}")
    List <ChatDiscussionsDTO> getAllUserDiscussions(@PathVariable("id") String id);
}

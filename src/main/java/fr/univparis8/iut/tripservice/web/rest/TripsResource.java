package fr.univparis8.iut.tripservice.web.rest;

import feign.FeignException;
import fr.univparis8.iut.tripservice.domain.User;
import fr.univparis8.iut.tripservice.service.TripsService;
import fr.univparis8.iut.tripservice.service.UserService;
import fr.univparis8.iut.tripservice.service.dto.FilterTripDTO;
import fr.univparis8.iut.tripservice.service.dto.JoinTripDTO;
import fr.univparis8.iut.tripservice.service.dto.TripDTO;
import fr.univparis8.iut.tripservice.service.dto.TripsDTO;
import fr.univparis8.iut.tripservice.service.dto.userservice.ChatDiscussionsDTO;
import fr.univparis8.iut.tripservice.web.rest.errors.BadRequestAlertException;
import fr.univparis8.iut.tripservice.web.rest.util.HeaderUtil;
import fr.univparis8.iut.tripservice.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.security.RolesAllowed;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class TripsResource {

    private final Logger log = LoggerFactory.getLogger(TripsResource.class);

    private final TripsService tripsService;

    private static final String ENTITY_NAME = "trip";

    private final UserService userService;

    private final UserServiceClient userServiceClient;

    public TripsResource(TripsService tripsService, UserService userService, UserServiceClient userServiceClient) {
        this.tripsService = tripsService;
        this.userService = userService;
        this.userServiceClient = userServiceClient;
    }

    @GetMapping("/voyages")
    public ResponseEntity<List<TripsDTO>> getAllTrips(Pageable pageable, @RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        Page<TripsDTO> page;
        if (eagerload) {
            page = tripsService.findAllWithEagerRelationships(pageable);
        } else {
            page = tripsService.findAll(pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, String.format("/api/trips?eagerload=%b", eagerload));
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }


    @GetMapping("/voyages/{id}")
    public ResponseEntity<TripsDTO> getTrip(@PathVariable String id) {
        Optional<TripsDTO> tripsDTO = tripsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(tripsDTO);
    }


    @GetMapping("/voyages/user")
    //@RolesAllowed({"CLIENT","PRO"})
    public List<TripsDTO> getUserTrips() {
        List<TripsDTO> tripsDTOS;
        User user = userService.getUserWithAuthorities().get();

        tripsDTOS = tripsService.findUserTrips(user.getId());

        return tripsDTOS;
    }

    @GetMapping("/voyages/userDiscussions")
    //@RolesAllowed({"CLIENT","PRO"})
    public List<ChatDiscussionsDTO> getUserDiscussion() {

        User user = userService.getUserWithAuthorities().get();

        List<TripsDTO> trips = tripsService.findUserTrips(user.getId());

        List<ChatDiscussionsDTO> chatDiscussionsDTOS = new ArrayList<>();

        //todo refactor to retreive all discusssion at once

        try {
            for (TripsDTO tripsDTO : trips) {
                List<ChatDiscussionsDTO> chatDiscussion = userServiceClient.getAllUserDiscussions(tripsDTO.getId());
                chatDiscussionsDTOS.addAll(chatDiscussion);
            }
        } catch (Exception e) {
            log.error("Error with userservice");
        }

        return chatDiscussionsDTOS;
    }

    @PostMapping("/voyages/joinTrip")
    public ResponseEntity addUserToTrip(@RequestBody JoinTripDTO joinTripDTO) {
        tripsService.addUserToTrip(joinTripDTO);

        return ResponseEntity.ok().headers(HeaderUtil.createAlert(
            "You joined the trip " + joinTripDTO.getTripId() + "sucessfully", joinTripDTO.getTripId()))
            .build();
    }

    @PostMapping("/voyages/new")
    public ResponseEntity<TripsDTO> createTrip(@RequestBody TripsDTO tripsDTO) throws URISyntaxException {
        if (tripsDTO.getId() != null) {
            throw new BadRequestAlertException("A new trip cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TripsDTO result = tripsService.save(tripsDTO);
        return ResponseEntity.created(new URI("/api/trips/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @PostMapping("/voyages/filter")
    public ResponseEntity<List<TripsDTO>> getFilterTrips(Pageable pageable,
                                                         @RequestParam(required = false, defaultValue = "false") boolean eagerload,
                                                         @RequestBody FilterTripDTO filterTripDTO) {
        Page<TripsDTO> page;
        if (eagerload) {
            page = null;
        } else {
            page = tripsService.findAllFilter(pageable, filterTripDTO);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, String.format("/api/trips?eagerload=%b", eagerload));
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/_search/voyages/{query}")
    public ResponseEntity<List<TripsDTO>> searchTrips(@PathVariable String query, Pageable pageable) {
        Page<TripsDTO> page = tripsService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/trips");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}

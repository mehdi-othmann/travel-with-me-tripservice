/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TravelWithMeTripServiceTestModule } from '../../../../test.module';
import { AdvertisementComponent } from 'app/entities/travelwithmeproservice/advertisement/advertisement.component';
import { AdvertisementService } from 'app/entities/travelwithmeproservice/advertisement/advertisement.service';
import { Advertisement } from 'app/shared/model/travelwithmeproservice/advertisement.model';

describe('Component Tests', () => {
    describe('Advertisement Management Component', () => {
        let comp: AdvertisementComponent;
        let fixture: ComponentFixture<AdvertisementComponent>;
        let service: AdvertisementService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [TravelWithMeTripServiceTestModule],
                declarations: [AdvertisementComponent],
                providers: []
            })
                .overrideTemplate(AdvertisementComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(AdvertisementComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AdvertisementService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Advertisement('123')],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.advertisements[0]).toEqual(jasmine.objectContaining({ id: '123' }));
        });
    });
});

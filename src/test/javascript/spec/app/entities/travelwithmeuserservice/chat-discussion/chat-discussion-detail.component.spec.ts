/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TravelWithMeTripServiceTestModule } from '../../../../test.module';
import { ChatDiscussionDetailComponent } from 'app/entities/travelwithmeuserservice/chat-discussion/chat-discussion-detail.component';
import { ChatDiscussion } from 'app/shared/model/travelwithmeuserservice/chat-discussion.model';

describe('Component Tests', () => {
    describe('ChatDiscussion Management Detail Component', () => {
        let comp: ChatDiscussionDetailComponent;
        let fixture: ComponentFixture<ChatDiscussionDetailComponent>;
        const route = ({ data: of({ chatDiscussion: new ChatDiscussion('123') }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [TravelWithMeTripServiceTestModule],
                declarations: [ChatDiscussionDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(ChatDiscussionDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ChatDiscussionDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.chatDiscussion).toEqual(jasmine.objectContaining({ id: '123' }));
            });
        });
    });
});

/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { TravelWithMeTripServiceTestModule } from '../../../../test.module';
import { ChatDiscussionUpdateComponent } from 'app/entities/travelwithmeuserservice/chat-discussion/chat-discussion-update.component';
import { ChatDiscussionService } from 'app/entities/travelwithmeuserservice/chat-discussion/chat-discussion.service';
import { ChatDiscussion } from 'app/shared/model/travelwithmeuserservice/chat-discussion.model';

describe('Component Tests', () => {
    describe('ChatDiscussion Management Update Component', () => {
        let comp: ChatDiscussionUpdateComponent;
        let fixture: ComponentFixture<ChatDiscussionUpdateComponent>;
        let service: ChatDiscussionService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [TravelWithMeTripServiceTestModule],
                declarations: [ChatDiscussionUpdateComponent]
            })
                .overrideTemplate(ChatDiscussionUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ChatDiscussionUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ChatDiscussionService);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity', fakeAsync(() => {
                // GIVEN
                const entity = new ChatDiscussion('123');
                spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.chatDiscussion = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.update).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));

            it('Should call create service on save for new entity', fakeAsync(() => {
                // GIVEN
                const entity = new ChatDiscussion();
                spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.chatDiscussion = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.create).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));
        });
    });
});

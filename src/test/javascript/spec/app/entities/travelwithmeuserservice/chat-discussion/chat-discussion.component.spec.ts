/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TravelWithMeTripServiceTestModule } from '../../../../test.module';
import { ChatDiscussionComponent } from 'app/entities/travelwithmeuserservice/chat-discussion/chat-discussion.component';
import { ChatDiscussionService } from 'app/entities/travelwithmeuserservice/chat-discussion/chat-discussion.service';
import { ChatDiscussion } from 'app/shared/model/travelwithmeuserservice/chat-discussion.model';

describe('Component Tests', () => {
    describe('ChatDiscussion Management Component', () => {
        let comp: ChatDiscussionComponent;
        let fixture: ComponentFixture<ChatDiscussionComponent>;
        let service: ChatDiscussionService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [TravelWithMeTripServiceTestModule],
                declarations: [ChatDiscussionComponent],
                providers: []
            })
                .overrideTemplate(ChatDiscussionComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ChatDiscussionComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ChatDiscussionService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new ChatDiscussion('123')],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.chatDiscussions[0]).toEqual(jasmine.objectContaining({ id: '123' }));
        });
    });
});

/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../../page-objects/jhi-page-objects';

import { AdvertisementComponentsPage, AdvertisementDeleteDialog, AdvertisementUpdatePage } from './advertisement.page-object';

const expect = chai.expect;

describe('Advertisement e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let advertisementUpdatePage: AdvertisementUpdatePage;
    let advertisementComponentsPage: AdvertisementComponentsPage;
    let advertisementDeleteDialog: AdvertisementDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Advertisements', async () => {
        await navBarPage.goToEntity('advertisement');
        advertisementComponentsPage = new AdvertisementComponentsPage();
        await browser.wait(ec.visibilityOf(advertisementComponentsPage.title), 5000);
        expect(await advertisementComponentsPage.getTitle()).to.eq('Advertisements');
    });

    it('should load create Advertisement page', async () => {
        await advertisementComponentsPage.clickOnCreateButton();
        advertisementUpdatePage = new AdvertisementUpdatePage();
        expect(await advertisementUpdatePage.getPageTitle()).to.eq('Create or edit a Advertisement');
        await advertisementUpdatePage.cancel();
    });

    it('should create and save Advertisements', async () => {
        const nbButtonsBeforeCreate = await advertisementComponentsPage.countDeleteButtons();

        await advertisementComponentsPage.clickOnCreateButton();
        await promise.all([
            advertisementUpdatePage.setAdvertisementTitleInput('advertisementTitle'),
            advertisementUpdatePage.setAdvertisementImageInput('advertisementImage'),
            advertisementUpdatePage.setOwnerInput('owner'),
            advertisementUpdatePage.setCreatedAtInput('2000-12-31'),
            advertisementUpdatePage.setUpdatedAtInput('2000-12-31'),
            advertisementUpdatePage.setExpirationDateInput('2000-12-31')
        ]);
        expect(await advertisementUpdatePage.getAdvertisementTitleInput()).to.eq('advertisementTitle');
        expect(await advertisementUpdatePage.getAdvertisementImageInput()).to.eq('advertisementImage');
        expect(await advertisementUpdatePage.getOwnerInput()).to.eq('owner');
        expect(await advertisementUpdatePage.getCreatedAtInput()).to.eq('2000-12-31');
        expect(await advertisementUpdatePage.getUpdatedAtInput()).to.eq('2000-12-31');
        expect(await advertisementUpdatePage.getExpirationDateInput()).to.eq('2000-12-31');
        await advertisementUpdatePage.save();
        expect(await advertisementUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await advertisementComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Advertisement', async () => {
        const nbButtonsBeforeDelete = await advertisementComponentsPage.countDeleteButtons();
        await advertisementComponentsPage.clickOnLastDeleteButton();

        advertisementDeleteDialog = new AdvertisementDeleteDialog();
        expect(await advertisementDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Advertisement?');
        await advertisementDeleteDialog.clickOnConfirmButton();

        expect(await advertisementComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});

import { element, by, ElementFinder } from 'protractor';

export class AdvertisementComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-advertisement div table .btn-danger'));
    title = element.all(by.css('jhi-advertisement div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getText();
    }
}

export class AdvertisementUpdatePage {
    pageTitle = element(by.id('jhi-advertisement-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    advertisementTitleInput = element(by.id('field_advertisementTitle'));
    advertisementImageInput = element(by.id('field_advertisementImage'));
    ownerInput = element(by.id('field_owner'));
    createdAtInput = element(by.id('field_createdAt'));
    updatedAtInput = element(by.id('field_updatedAt'));
    expirationDateInput = element(by.id('field_expirationDate'));

    async getPageTitle() {
        return this.pageTitle.getText();
    }

    async setAdvertisementTitleInput(advertisementTitle) {
        await this.advertisementTitleInput.sendKeys(advertisementTitle);
    }

    async getAdvertisementTitleInput() {
        return this.advertisementTitleInput.getAttribute('value');
    }

    async setAdvertisementImageInput(advertisementImage) {
        await this.advertisementImageInput.sendKeys(advertisementImage);
    }

    async getAdvertisementImageInput() {
        return this.advertisementImageInput.getAttribute('value');
    }

    async setOwnerInput(owner) {
        await this.ownerInput.sendKeys(owner);
    }

    async getOwnerInput() {
        return this.ownerInput.getAttribute('value');
    }

    async setCreatedAtInput(createdAt) {
        await this.createdAtInput.sendKeys(createdAt);
    }

    async getCreatedAtInput() {
        return this.createdAtInput.getAttribute('value');
    }

    async setUpdatedAtInput(updatedAt) {
        await this.updatedAtInput.sendKeys(updatedAt);
    }

    async getUpdatedAtInput() {
        return this.updatedAtInput.getAttribute('value');
    }

    async setExpirationDateInput(expirationDate) {
        await this.expirationDateInput.sendKeys(expirationDate);
    }

    async getExpirationDateInput() {
        return this.expirationDateInput.getAttribute('value');
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class AdvertisementDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-advertisement-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-advertisement'));

    async getDialogTitle() {
        return this.dialogTitle.getText();
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}

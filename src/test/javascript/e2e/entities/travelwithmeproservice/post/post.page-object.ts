import { element, by, ElementFinder } from 'protractor';

export class PostComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-post div table .btn-danger'));
    title = element.all(by.css('jhi-post div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getText();
    }
}

export class PostUpdatePage {
    pageTitle = element(by.id('jhi-post-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    postTitleInput = element(by.id('field_postTitle'));
    postContentsInput = element(by.id('field_postContents'));
    ownerInput = element(by.id('field_owner'));

    async getPageTitle() {
        return this.pageTitle.getText();
    }

    async setPostTitleInput(postTitle) {
        await this.postTitleInput.sendKeys(postTitle);
    }

    async getPostTitleInput() {
        return this.postTitleInput.getAttribute('value');
    }

    async setPostContentsInput(postContents) {
        await this.postContentsInput.sendKeys(postContents);
    }

    async getPostContentsInput() {
        return this.postContentsInput.getAttribute('value');
    }

    async setOwnerInput(owner) {
        await this.ownerInput.sendKeys(owner);
    }

    async getOwnerInput() {
        return this.ownerInput.getAttribute('value');
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class PostDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-post-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-post'));

    async getDialogTitle() {
        return this.dialogTitle.getText();
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}

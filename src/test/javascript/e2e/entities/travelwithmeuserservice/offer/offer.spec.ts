/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../../page-objects/jhi-page-objects';

import { OfferComponentsPage, OfferDeleteDialog, OfferUpdatePage } from './offer.page-object';

const expect = chai.expect;

describe('Offer e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let offerUpdatePage: OfferUpdatePage;
    let offerComponentsPage: OfferComponentsPage;
    let offerDeleteDialog: OfferDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Offers', async () => {
        await navBarPage.goToEntity('offer');
        offerComponentsPage = new OfferComponentsPage();
        await browser.wait(ec.visibilityOf(offerComponentsPage.title), 5000);
        expect(await offerComponentsPage.getTitle()).to.eq('Offers');
    });

    it('should load create Offer page', async () => {
        await offerComponentsPage.clickOnCreateButton();
        offerUpdatePage = new OfferUpdatePage();
        expect(await offerUpdatePage.getPageTitle()).to.eq('Create or edit a Offer');
        await offerUpdatePage.cancel();
    });

    it('should create and save Offers', async () => {
        const nbButtonsBeforeCreate = await offerComponentsPage.countDeleteButtons();

        await offerComponentsPage.clickOnCreateButton();
        await promise.all([offerUpdatePage.setNameInput('name'), offerUpdatePage.setPriceInput('5')]);
        expect(await offerUpdatePage.getNameInput()).to.eq('name');
        expect(await offerUpdatePage.getPriceInput()).to.eq('5');
        await offerUpdatePage.save();
        expect(await offerUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await offerComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Offer', async () => {
        const nbButtonsBeforeDelete = await offerComponentsPage.countDeleteButtons();
        await offerComponentsPage.clickOnLastDeleteButton();

        offerDeleteDialog = new OfferDeleteDialog();
        expect(await offerDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Offer?');
        await offerDeleteDialog.clickOnConfirmButton();

        expect(await offerComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});

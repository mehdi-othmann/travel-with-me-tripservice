/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../../page-objects/jhi-page-objects';

import { ChatMessageComponentsPage, ChatMessageDeleteDialog, ChatMessageUpdatePage } from './chat-message.page-object';

const expect = chai.expect;

describe('ChatMessage e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let chatMessageUpdatePage: ChatMessageUpdatePage;
    let chatMessageComponentsPage: ChatMessageComponentsPage;
    let chatMessageDeleteDialog: ChatMessageDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load ChatMessages', async () => {
        await navBarPage.goToEntity('chat-message');
        chatMessageComponentsPage = new ChatMessageComponentsPage();
        await browser.wait(ec.visibilityOf(chatMessageComponentsPage.title), 5000);
        expect(await chatMessageComponentsPage.getTitle()).to.eq('Chat Messages');
    });

    it('should load create ChatMessage page', async () => {
        await chatMessageComponentsPage.clickOnCreateButton();
        chatMessageUpdatePage = new ChatMessageUpdatePage();
        expect(await chatMessageUpdatePage.getPageTitle()).to.eq('Create or edit a Chat Message');
        await chatMessageUpdatePage.cancel();
    });

    it('should create and save ChatMessages', async () => {
        const nbButtonsBeforeCreate = await chatMessageComponentsPage.countDeleteButtons();

        await chatMessageComponentsPage.clickOnCreateButton();
        await promise.all([
            chatMessageUpdatePage.setContentInput('content'),
            chatMessageUpdatePage.setChatDiscussionIdInput('chatDiscussionId')
        ]);
        expect(await chatMessageUpdatePage.getContentInput()).to.eq('content');
        expect(await chatMessageUpdatePage.getChatDiscussionIdInput()).to.eq('chatDiscussionId');
        await chatMessageUpdatePage.save();
        expect(await chatMessageUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await chatMessageComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last ChatMessage', async () => {
        const nbButtonsBeforeDelete = await chatMessageComponentsPage.countDeleteButtons();
        await chatMessageComponentsPage.clickOnLastDeleteButton();

        chatMessageDeleteDialog = new ChatMessageDeleteDialog();
        expect(await chatMessageDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Chat Message?');
        await chatMessageDeleteDialog.clickOnConfirmButton();

        expect(await chatMessageComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});

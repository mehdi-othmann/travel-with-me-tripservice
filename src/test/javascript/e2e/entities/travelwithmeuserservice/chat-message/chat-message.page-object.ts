import { element, by, ElementFinder } from 'protractor';

export class ChatMessageComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-chat-message div table .btn-danger'));
    title = element.all(by.css('jhi-chat-message div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getText();
    }
}

export class ChatMessageUpdatePage {
    pageTitle = element(by.id('jhi-chat-message-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    contentInput = element(by.id('field_content'));
    chatDiscussionIdInput = element(by.id('field_chatDiscussionId'));

    async getPageTitle() {
        return this.pageTitle.getText();
    }

    async setContentInput(content) {
        await this.contentInput.sendKeys(content);
    }

    async getContentInput() {
        return this.contentInput.getAttribute('value');
    }

    async setChatDiscussionIdInput(chatDiscussionId) {
        await this.chatDiscussionIdInput.sendKeys(chatDiscussionId);
    }

    async getChatDiscussionIdInput() {
        return this.chatDiscussionIdInput.getAttribute('value');
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class ChatMessageDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-chatMessage-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-chatMessage'));

    async getDialogTitle() {
        return this.dialogTitle.getText();
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}

/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../../page-objects/jhi-page-objects';

import { ChatDiscussionComponentsPage, ChatDiscussionDeleteDialog, ChatDiscussionUpdatePage } from './chat-discussion.page-object';

const expect = chai.expect;

describe('ChatDiscussion e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let chatDiscussionUpdatePage: ChatDiscussionUpdatePage;
    let chatDiscussionComponentsPage: ChatDiscussionComponentsPage;
    let chatDiscussionDeleteDialog: ChatDiscussionDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load ChatDiscussions', async () => {
        await navBarPage.goToEntity('chat-discussion');
        chatDiscussionComponentsPage = new ChatDiscussionComponentsPage();
        await browser.wait(ec.visibilityOf(chatDiscussionComponentsPage.title), 5000);
        expect(await chatDiscussionComponentsPage.getTitle()).to.eq('Chat Discussions');
    });

    it('should load create ChatDiscussion page', async () => {
        await chatDiscussionComponentsPage.clickOnCreateButton();
        chatDiscussionUpdatePage = new ChatDiscussionUpdatePage();
        expect(await chatDiscussionUpdatePage.getPageTitle()).to.eq('Create or edit a Chat Discussion');
        await chatDiscussionUpdatePage.cancel();
    });

    it('should create and save ChatDiscussions', async () => {
        const nbButtonsBeforeCreate = await chatDiscussionComponentsPage.countDeleteButtons();

        await chatDiscussionComponentsPage.clickOnCreateButton();
        await promise.all([chatDiscussionUpdatePage.setTitleInput('title'), chatDiscussionUpdatePage.setTripInput('trip')]);
        expect(await chatDiscussionUpdatePage.getTitleInput()).to.eq('title');
        expect(await chatDiscussionUpdatePage.getTripInput()).to.eq('trip');
        await chatDiscussionUpdatePage.save();
        expect(await chatDiscussionUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await chatDiscussionComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last ChatDiscussion', async () => {
        const nbButtonsBeforeDelete = await chatDiscussionComponentsPage.countDeleteButtons();
        await chatDiscussionComponentsPage.clickOnLastDeleteButton();

        chatDiscussionDeleteDialog = new ChatDiscussionDeleteDialog();
        expect(await chatDiscussionDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Chat Discussion?');
        await chatDiscussionDeleteDialog.clickOnConfirmButton();

        expect(await chatDiscussionComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});

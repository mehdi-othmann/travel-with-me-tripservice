import { element, by, ElementFinder } from 'protractor';

export class ChatDiscussionComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-chat-discussion div table .btn-danger'));
    title = element.all(by.css('jhi-chat-discussion div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getText();
    }
}

export class ChatDiscussionUpdatePage {
    pageTitle = element(by.id('jhi-chat-discussion-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    titleInput = element(by.id('field_title'));
    tripInput = element(by.id('field_trip'));

    async getPageTitle() {
        return this.pageTitle.getText();
    }

    async setTitleInput(title) {
        await this.titleInput.sendKeys(title);
    }

    async getTitleInput() {
        return this.titleInput.getAttribute('value');
    }

    async setTripInput(trip) {
        await this.tripInput.sendKeys(trip);
    }

    async getTripInput() {
        return this.tripInput.getAttribute('value');
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class ChatDiscussionDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-chatDiscussion-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-chatDiscussion'));

    async getDialogTitle() {
        return this.dialogTitle.getText();
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}

/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { TripComponentsPage, TripDeleteDialog, TripUpdatePage } from './trip.page-object';

const expect = chai.expect;

describe('Trip e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let tripUpdatePage: TripUpdatePage;
    let tripComponentsPage: TripComponentsPage;
    let tripDeleteDialog: TripDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Trips', async () => {
        await navBarPage.goToEntity('trip');
        tripComponentsPage = new TripComponentsPage();
        await browser.wait(ec.visibilityOf(tripComponentsPage.title), 5000);
        expect(await tripComponentsPage.getTitle()).to.eq('Trips');
    });

    it('should load create Trip page', async () => {
        await tripComponentsPage.clickOnCreateButton();
        tripUpdatePage = new TripUpdatePage();
        expect(await tripUpdatePage.getPageTitle()).to.eq('Create or edit a Trip');
        await tripUpdatePage.cancel();
    });

    it('should create and save Trips', async () => {
        const nbButtonsBeforeCreate = await tripComponentsPage.countDeleteButtons();

        await tripComponentsPage.clickOnCreateButton();
        await promise.all([
            tripUpdatePage.setTitleInput('title'),
            tripUpdatePage.setDescriptionInput('description'),
            tripUpdatePage.setDurationInput('duration'),
            tripUpdatePage.setSizeInput('5'),
            tripUpdatePage.setPeriodeInput('2000-12-31'),
            tripUpdatePage.setCreatedAtInput('2000-12-31'),
            tripUpdatePage.setUpdatedAtInput('2000-12-31'),
            tripUpdatePage.setAproxPriceInput('5'),
            tripUpdatePage.setImageInput('image'),
            tripUpdatePage.countrySelectLastOption(),
            // tripUpdatePage.categoriesSelectLastOption(),
            tripUpdatePage.ownerSelectLastOption()
            // tripUpdatePage.usersSelectLastOption(),
        ]);
        expect(await tripUpdatePage.getTitleInput()).to.eq('title');
        expect(await tripUpdatePage.getDescriptionInput()).to.eq('description');
        expect(await tripUpdatePage.getDurationInput()).to.eq('duration');
        expect(await tripUpdatePage.getSizeInput()).to.eq('5');
        expect(await tripUpdatePage.getPeriodeInput()).to.eq('2000-12-31');
        expect(await tripUpdatePage.getCreatedAtInput()).to.eq('2000-12-31');
        expect(await tripUpdatePage.getUpdatedAtInput()).to.eq('2000-12-31');
        expect(await tripUpdatePage.getAproxPriceInput()).to.eq('5');
        expect(await tripUpdatePage.getImageInput()).to.eq('image');
        await tripUpdatePage.save();
        expect(await tripUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await tripComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Trip', async () => {
        const nbButtonsBeforeDelete = await tripComponentsPage.countDeleteButtons();
        await tripComponentsPage.clickOnLastDeleteButton();

        tripDeleteDialog = new TripDeleteDialog();
        expect(await tripDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this Trip?');
        await tripDeleteDialog.clickOnConfirmButton();

        expect(await tripComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});

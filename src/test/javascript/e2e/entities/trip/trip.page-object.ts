import { element, by, ElementFinder } from 'protractor';

export class TripComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-trip div table .btn-danger'));
    title = element.all(by.css('jhi-trip div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getText();
    }
}

export class TripUpdatePage {
    pageTitle = element(by.id('jhi-trip-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    titleInput = element(by.id('field_title'));
    descriptionInput = element(by.id('field_description'));
    durationInput = element(by.id('field_duration'));
    sizeInput = element(by.id('field_size'));
    periodeInput = element(by.id('field_periode'));
    createdAtInput = element(by.id('field_createdAt'));
    updatedAtInput = element(by.id('field_updatedAt'));
    aproxPriceInput = element(by.id('field_aproxPrice'));
    imageInput = element(by.id('field_image'));
    countrySelect = element(by.id('field_country'));
    categoriesSelect = element(by.id('field_categories'));
    ownerSelect = element(by.id('field_owner'));
    usersSelect = element(by.id('field_users'));

    async getPageTitle() {
        return this.pageTitle.getText();
    }

    async setTitleInput(title) {
        await this.titleInput.sendKeys(title);
    }

    async getTitleInput() {
        return this.titleInput.getAttribute('value');
    }

    async setDescriptionInput(description) {
        await this.descriptionInput.sendKeys(description);
    }

    async getDescriptionInput() {
        return this.descriptionInput.getAttribute('value');
    }

    async setDurationInput(duration) {
        await this.durationInput.sendKeys(duration);
    }

    async getDurationInput() {
        return this.durationInput.getAttribute('value');
    }

    async setSizeInput(size) {
        await this.sizeInput.sendKeys(size);
    }

    async getSizeInput() {
        return this.sizeInput.getAttribute('value');
    }

    async setPeriodeInput(periode) {
        await this.periodeInput.sendKeys(periode);
    }

    async getPeriodeInput() {
        return this.periodeInput.getAttribute('value');
    }

    async setCreatedAtInput(createdAt) {
        await this.createdAtInput.sendKeys(createdAt);
    }

    async getCreatedAtInput() {
        return this.createdAtInput.getAttribute('value');
    }

    async setUpdatedAtInput(updatedAt) {
        await this.updatedAtInput.sendKeys(updatedAt);
    }

    async getUpdatedAtInput() {
        return this.updatedAtInput.getAttribute('value');
    }

    async setAproxPriceInput(aproxPrice) {
        await this.aproxPriceInput.sendKeys(aproxPrice);
    }

    async getAproxPriceInput() {
        return this.aproxPriceInput.getAttribute('value');
    }

    async setImageInput(image) {
        await this.imageInput.sendKeys(image);
    }

    async getImageInput() {
        return this.imageInput.getAttribute('value');
    }

    async countrySelectLastOption() {
        await this.countrySelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async countrySelectOption(option) {
        await this.countrySelect.sendKeys(option);
    }

    getCountrySelect(): ElementFinder {
        return this.countrySelect;
    }

    async getCountrySelectedOption() {
        return this.countrySelect.element(by.css('option:checked')).getText();
    }

    async categoriesSelectLastOption() {
        await this.categoriesSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async categoriesSelectOption(option) {
        await this.categoriesSelect.sendKeys(option);
    }

    getCategoriesSelect(): ElementFinder {
        return this.categoriesSelect;
    }

    async getCategoriesSelectedOption() {
        return this.categoriesSelect.element(by.css('option:checked')).getText();
    }

    async ownerSelectLastOption() {
        await this.ownerSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async ownerSelectOption(option) {
        await this.ownerSelect.sendKeys(option);
    }

    getOwnerSelect(): ElementFinder {
        return this.ownerSelect;
    }

    async getOwnerSelectedOption() {
        return this.ownerSelect.element(by.css('option:checked')).getText();
    }

    async usersSelectLastOption() {
        await this.usersSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async usersSelectOption(option) {
        await this.usersSelect.sendKeys(option);
    }

    getUsersSelect(): ElementFinder {
        return this.usersSelect;
    }

    async getUsersSelectedOption() {
        return this.usersSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class TripDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-trip-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-trip'));

    async getDialogTitle() {
        return this.dialogTitle.getText();
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}

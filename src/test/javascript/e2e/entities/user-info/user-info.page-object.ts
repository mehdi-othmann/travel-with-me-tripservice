import { element, by, ElementFinder } from 'protractor';

export class UserInfoComponentsPage {
    createButton = element(by.id('jh-create-entity'));
    deleteButtons = element.all(by.css('jhi-user-info div table .btn-danger'));
    title = element.all(by.css('jhi-user-info div h2#page-heading span')).first();

    async clickOnCreateButton() {
        await this.createButton.click();
    }

    async clickOnLastDeleteButton() {
        await this.deleteButtons.last().click();
    }

    async countDeleteButtons() {
        return this.deleteButtons.count();
    }

    async getTitle() {
        return this.title.getText();
    }
}

export class UserInfoUpdatePage {
    pageTitle = element(by.id('jhi-user-info-heading'));
    saveButton = element(by.id('save-entity'));
    cancelButton = element(by.id('cancel-save'));
    descriptionInput = element(by.id('field_description'));
    birthDayInput = element(by.id('field_birthDay'));
    jobInput = element(by.id('field_job'));
    favoriteArtistInput = element(by.id('field_favoriteArtist'));
    favoriteBookInput = element(by.id('field_favoriteBook'));
    userSelect = element(by.id('field_user'));

    async getPageTitle() {
        return this.pageTitle.getText();
    }

    async setDescriptionInput(description) {
        await this.descriptionInput.sendKeys(description);
    }

    async getDescriptionInput() {
        return this.descriptionInput.getAttribute('value');
    }

    async setBirthDayInput(birthDay) {
        await this.birthDayInput.sendKeys(birthDay);
    }

    async getBirthDayInput() {
        return this.birthDayInput.getAttribute('value');
    }

    async setJobInput(job) {
        await this.jobInput.sendKeys(job);
    }

    async getJobInput() {
        return this.jobInput.getAttribute('value');
    }

    async setFavoriteArtistInput(favoriteArtist) {
        await this.favoriteArtistInput.sendKeys(favoriteArtist);
    }

    async getFavoriteArtistInput() {
        return this.favoriteArtistInput.getAttribute('value');
    }

    async setFavoriteBookInput(favoriteBook) {
        await this.favoriteBookInput.sendKeys(favoriteBook);
    }

    async getFavoriteBookInput() {
        return this.favoriteBookInput.getAttribute('value');
    }

    async userSelectLastOption() {
        await this.userSelect
            .all(by.tagName('option'))
            .last()
            .click();
    }

    async userSelectOption(option) {
        await this.userSelect.sendKeys(option);
    }

    getUserSelect(): ElementFinder {
        return this.userSelect;
    }

    async getUserSelectedOption() {
        return this.userSelect.element(by.css('option:checked')).getText();
    }

    async save() {
        await this.saveButton.click();
    }

    async cancel() {
        await this.cancelButton.click();
    }

    getSaveButton(): ElementFinder {
        return this.saveButton;
    }
}

export class UserInfoDeleteDialog {
    private dialogTitle = element(by.id('jhi-delete-userInfo-heading'));
    private confirmButton = element(by.id('jhi-confirm-delete-userInfo'));

    async getDialogTitle() {
        return this.dialogTitle.getText();
    }

    async clickOnConfirmButton() {
        await this.confirmButton.click();
    }
}

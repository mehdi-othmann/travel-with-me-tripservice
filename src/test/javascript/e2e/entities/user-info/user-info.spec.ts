/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { UserInfoComponentsPage, UserInfoDeleteDialog, UserInfoUpdatePage } from './user-info.page-object';

const expect = chai.expect;

describe('UserInfo e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let userInfoUpdatePage: UserInfoUpdatePage;
    let userInfoComponentsPage: UserInfoComponentsPage;
    let userInfoDeleteDialog: UserInfoDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load UserInfos', async () => {
        await navBarPage.goToEntity('user-info');
        userInfoComponentsPage = new UserInfoComponentsPage();
        await browser.wait(ec.visibilityOf(userInfoComponentsPage.title), 5000);
        expect(await userInfoComponentsPage.getTitle()).to.eq('User Infos');
    });

    it('should load create UserInfo page', async () => {
        await userInfoComponentsPage.clickOnCreateButton();
        userInfoUpdatePage = new UserInfoUpdatePage();
        expect(await userInfoUpdatePage.getPageTitle()).to.eq('Create or edit a User Info');
        await userInfoUpdatePage.cancel();
    });

    it('should create and save UserInfos', async () => {
        const nbButtonsBeforeCreate = await userInfoComponentsPage.countDeleteButtons();

        await userInfoComponentsPage.clickOnCreateButton();
        await promise.all([
            userInfoUpdatePage.setDescriptionInput('description'),
            userInfoUpdatePage.setBirthDayInput('2000-12-31'),
            userInfoUpdatePage.setJobInput('job'),
            userInfoUpdatePage.setFavoriteArtistInput('favoriteArtist'),
            userInfoUpdatePage.setFavoriteBookInput('favoriteBook'),
            userInfoUpdatePage.userSelectLastOption()
        ]);
        expect(await userInfoUpdatePage.getDescriptionInput()).to.eq('description');
        expect(await userInfoUpdatePage.getBirthDayInput()).to.eq('2000-12-31');
        expect(await userInfoUpdatePage.getJobInput()).to.eq('job');
        expect(await userInfoUpdatePage.getFavoriteArtistInput()).to.eq('favoriteArtist');
        expect(await userInfoUpdatePage.getFavoriteBookInput()).to.eq('favoriteBook');
        await userInfoUpdatePage.save();
        expect(await userInfoUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await userInfoComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last UserInfo', async () => {
        const nbButtonsBeforeDelete = await userInfoComponentsPage.countDeleteButtons();
        await userInfoComponentsPage.clickOnLastDeleteButton();

        userInfoDeleteDialog = new UserInfoDeleteDialog();
        expect(await userInfoDeleteDialog.getDialogTitle()).to.eq('Are you sure you want to delete this User Info?');
        await userInfoDeleteDialog.clickOnConfirmButton();

        expect(await userInfoComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
